# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018 Bruno Laurencich
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client.  If not, see <https://www.gnu.org/licenses/>.
#
#
# This program uses code from various sources, the default license is GNU GPLv3
# for all code, the dependencies where originally distributed as follows:
# -dnspython library: Copyright 2003-2007, 2009, 2011 Nominum, Inc. Under ISC License
# -osc4py3 library: Copyright 2013-2018 CNRS. Under CeCILL-2.1 license
# -request library: Copyright: 2017 Kenneth Reitz. Under Apache 2.0 License 
#

import bpy
from mathutils import Quaternion
import pdb

from sys import path
from os.path import join, abspath, dirname
path.append(join(dirname(abspath(__file__)), "osc4py3-fork"))
from osc4py3.as_comthreads import *
from osc4py3 import oscmethod as osm
from osc4py3 import oscchannel as osch
from re import compile

# from struct import pack as struct_pack
# import socket

if "u" in locals():
    import imp
    imp.reload(u)
    imp.reload(arm)
    imp.reload(send)
else:
    from . import utils as u
    from . import armature as arm
    from . import send


D = bpy.data

class Chord_Stop_Receive_OSC(bpy.types.Operator):
    """Tries to get the ip of the notochord"""
    bl_idname = "chordata.stop_receive"
    bl_label = "Chordata: Cancel Pose Data reception"
    bl_options = {'REGISTER'}

    @classmethod
    def poll(cls, context):
        return context.scene.chordata.target != None

    def execute(self, context):
        context.user_preferences.addons[__package__].preferences.receiving = False
        return {"FINISHED"}

class Chord_Clean_Anims(bpy.types.Operator):
    """Unlink actions of the armature, and leave them with fake users"""
    bl_idname = "chordata.clean_anims"
    bl_label = "Chordata: Clean previous animations"
    bl_options = {'REGISTER'}

    @classmethod
    def poll(cls, context):
        return context.scene.chordata.target != None

    def clean_prev_animations(self):      
              
        self.context.scene.frame_current = 0
        try:
            self.armature.animation_data.action.use_fake_user = True
            self.armature.animation_data_clear()
        except AttributeError:
            u.write_blender_console(self.context, "No previous animation data found.")

    def execute(self, context):
        
        self.context = context
        self.armature = context.scene.chordata.target
        self.clean_prev_animations()
        return {"FINISHED"}

class Receive_Base:
    # -----------  UTILS  -----------
    def inform(self, type, message):
        try:
            self.report(type, message)
        except AttributeError:
            print("|| [{}] {} ||".format(type, message))    

    def prepare_armature(self):
        bpy.ops.chordata.clean_anims()
        self.armature.reset_pose()

        self.chord_global.do_set_keyframes = False
        self.context.scene.frame_current = 0
        self.context.scene.frame_start = 0
        self.text(self.armature.message)
    
    def text(self, text = None, header = True):
        if text:
            u.write_blender_console(self.context, text)
            if header:
                for a in self.views_3d:
                    a.header_text_set(text)
        else:
            for a in self.views_3d:
                a.header_text_set()

    def start_stop_animation(self):
        if self.chord_global.do_set_keyframes:
            self.chord_global.do_set_keyframes = False
            self.inform({"INFO"}, "Stop recording animation")

        else:
            bpy.ops.chordata.clean_anims()
            self.chord_global.do_set_keyframes = True
            self.inform({"INFO"}, "Recording animation!")
        

    def set_keyframe(self):
        if self.chord_global.do_set_keyframes:
            self.armature.set_key(self.context)
            self.context.scene.frame_current += 1


    # -----------  SERVER  -----------

    def start_server(self):
        osc_startup()
        method = self.chord_global.trans_method
        server = u.transmition_methods(False)[method][2]
        addr = "0.0.0.0" 
        if method == "multi": 
            addr = u.get_transmition_addr(self.chord_global)
        
        port= self.chord_global.notochord_dest_port

        try:
            server(addr, port, "chordata_armature_server")
        except Exception as e:
            u.write_blender_console(self.context, str(e))
            self.inform({"ERROR"}, "There was a problem creating the UDP server")
            return False

        msg = "Receiving in %s:%d (%s)" % \
            (addr, port, u.transmition_methods(False)[method][0])
        print(msg)
        try:
            u.write_blender_console(bpy.context, msg)
        except Exception as e:
            print("Couldn't write to internal console..")
        
        #TODO: work on the OSC routing, the library is not using regex but glob??
        bone_handler = self.receive_bones
        if self.chord_global.debug_mode: bone_handler = self.receive_bones_verbose
        osc_method("/Chordata/q/*", bone_handler,\
            argscheme=osm.OSCARG_ADDRESS + osm.OSCARG_DATA)

        osc_method("/Chordata/cmd", self.receive_cmd,\
            argscheme=osm.OSCARG_DATA)

        self.inform({"INFO"}, "Receiving OSC")
        return True

    def terminate_server(self):
        server = osch.get_channel("chordata_armature_server")
        if server:
            server.terminate()
            send.clean()
            self.chord_global.receiving = False
            self.chord_global.do_set_keyframes = False
            osc_terminate()

    def _do_receive_cmd(self, values):
        print("Received OSC command. Attention, handler not set! ", values)

    def receive_cmd(self, values):
        self._do_receive_cmd(values)

    def receive_bones(self, addr, values):
        sensor = self.chord_regex.search(addr)
        if not sensor or sensor.lastindex < 1:
            print("Invalid address", addr)
            return
        self.armature.put_quad_on_bones(sensor.group(1), Quaternion(values))

    def receive_bones_verbose(self, addr, values):
        sensor = self.chord_regex.search(addr)
        if not sensor or sensor.lastindex < 1:
            print("Invalid address", addr, values)
            return
        print("Received:", addr, values)
        self.armature.put_quad_on_bones(sensor.group(1), Quaternion(values))


class Chordata_Receive_OSC( Receive_Base ):
    # -----------  INIT  -----------
    def initialize(self, context):
        self.chord_global = context.user_preferences.addons[__package__].preferences
        self.context = context
        self.views_3d =  [area for area in bpy.context.screen.areas if area.type == 'VIEW_3D' ] 
        #is blender running headless or with a GUI?
        self.running_headless = bpy.app.background

        if not context.scene.chordata.target:
            self.inform({"ERROR"}, "No armature object selected")
            return {"CANCELLED"}
        
        chord_scene = context.scene.chordata
        self.armature = arm.Armature_Handler(chord_scene.target, chord_scene.helpers)

        self.prepare_armature()

        self.chord_global = context.user_preferences.addons[__package__].preferences
        self.chord_global.receiving = True
        self.chord_regex = compile(self.chord_global.capture_quat_patt)

        if not self.running_headless:
            wm = context.window_manager
            self._timer = wm.event_timer_add( \
                                1 / context.scene.render.fps,\
                                context.window)
            wm.modal_handler_add(self)

        if not self.start_server(): return {"CANCELLED"}

        self.chord_global.send.net_submask = self.chord_global.net_submask 
        send.init_osc(self.chord_global.send)

        self.keep_running = True

        return {'RUNNING_MODAL'}

    
    # -----------  CANCEL  -----------
    def cancel(self, context):
        del self.armature
        self.context.scene.frame_end = \
            self.context.scene.frame_current
        self.context.scene.frame_current = 0

        if not self.running_headless:
            wm = context.window_manager
            wm.event_timer_remove(self._timer)
        
        self.keep_running = False
        self.terminate_server()
        self.text(None)

    # -----------  HANDLE OSC CMDs  -----------
    
    def _do_receive_cmd(self, values):
        """List of comands:
        0 = CLOSE
        1 = END_OF_POSE_CALIBRATION
        """
        command_code, command_message = values
        
        if command_code == 0:
            self.cancel(bpy.context)

        elif command_code == 1:
            self.end_of_calibration()

        else:
            self.text("Received unknown CMD: {} - {}".format(command_code, command_message), header = False)
            return            

        self.text("Executed CMD: {} - {}".format(command_code, command_message), header = False)

    # -----------  MODAL FNs  -----------
    def process_message(self):
        osc_process()
        self.set_keyframe()
        send.send_Armature(self.armature, self.chord_global.send)

    def end_of_calibration(self):
        self.armature.get_rot_diff()
        self.text(self.armature.message)
        



class _OP_Chordata_Receive_OSC(bpy.types.Operator, Chordata_Receive_OSC):
    """Receives the capture and puts it on an armature"""
    bl_idname = "chordata.receive"
    bl_label = "Chordata: Receive pose Data"
    bl_options = {'REGISTER'}

    @classmethod
    def poll(cls, context):
        return context.scene.chordata.target != None \
                and context.scene.chordata.target.type == "ARMATURE"

    # -----------  MODAL  -----------
    def modal(self, context, event):
        if event.type == 'TIMER':
            self.process_message()

        elif event.type == 'RET' and event.value == "RELEASE":
            self.end_of_calibration()

        elif event.type == "K" and event.value == "RELEASE":
            self.start_stop_animation()

        elif event.type in {'RIGHTMOUSE', 'ESC'} or not self.chord_global.receiving:
            if self.chord_global.manage_notochord:
                bpy.ops.chordata.close_notochord("EXEC_DEFAULT")
            self.inform({"WARNING"}, "Connection canceled")
            print(" <-- Connection canceled --> ")
            self.cancel(context)
            return {'CANCELLED'}

        return {'PASS_THROUGH'}

    # -----------  INVOKE  -----------

    def invoke(self, context, event):
        self.chord_global = context.user_preferences.addons[__package__].preferences

        if self.chord_global.manage_notochord:
            bpy.ops.chordata.start_notochord("INVOKE_DEFAULT")
            
        return self.execute(context)

    # -----------  EXECUTE  -----------

    def execute(self, context):
        return self.initialize(context)

    

