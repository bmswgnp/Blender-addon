# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018 Bruno Laurencich
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client.  If not, see <https://www.gnu.org/licenses/>.
#
#
# This program uses code from various sources, the default license is GNU GPLv3
# for all code, the dependencies where originally distributed as follows:
# -dnspython library: Copyright 2003-2007, 2009, 2011 Nominum, Inc. Under ISC License
# -osc4py3 library: Copyright 2013-2018 CNRS. Under CeCILL-2.1 license
# -request library: Copyright: 2017 Kenneth Reitz. Under Apache 2.0 License 
#

import bpy
import threading

if "mdns" in locals():
	import imp
	imp.reload(mdns)
	imp.reload(u)
else:
	from . import mdns
	from . import utils as u




# =====================================================
# =         OPERATOR: GET THE NOTOCHORD IP            =
# =====================================================

class Chord_Get_Ip(bpy.types.Operator):
	"""Tries to get the ip of the notochord"""
	bl_idname = "chordata.get_ip"
	bl_label = "Chordata: get notochord ip"
	bl_options = {'REGISTER'}

	# -----------  PROPS  -----------
	counter = 0

	# -----------  MODAL  -----------

	def modal(self, context, event):
		if event.type in {'RIGHTMOUSE', 'ESC'}:
			self.report({"WARNING"}, "Connection canceled")
			self.cancel(context)
			with self.query_lock:
				if not self.found_ip:
					self.chord_global.state = u.possible_states()[0][0]

			return {'CANCELLED'}

		if event.type == 'TIMER':
			context.area.tag_redraw()

			if self.query_thread.is_alive():		
				self.chord_global.state = \
					u.possible_states()[self.counter%2+3][0]
			else:
				with self.query_lock:
					if self.found_ip:
						self.chord_global.state = u.possible_states()[1][0]
						self.report({"INFO"}, "IP found :" + self.found_ip)
						self.chord_global.notochord_ip = self.found_ip

					else:	
						self.chord_global.state = u.possible_states()[0][0]
						self.report({"WARNING"}, "IP NOT found")
					

				self.cancel(context)
				return {'FINISHED'}

			self.counter += 1
		
		return {'PASS_THROUGH'}

	# -----------  DO QUERY (thread handler)  -----------

	def do_query(self):
		self.report({"DEBUG"}, "Query Thread running.")
		ip = mdns.get_ip(self.chord_global.notochord_hostname+".local")
		max_attempts = self.chord_global.max_ip_discover_attempts
		if max_attempts == 0: max_attempts = 100 
		n = 1
		while not ip and n < max_attempts:
			ip = mdns.get_ip(self.chord_global.notochord_hostname+".local")
			if ip: break

			mdns.set_timeout(mdns.timeout * 2)
			
			with self.query_lock:
				self.report({"INFO"},\
					"ip not found, retrying in %d secs" %mdns.timeout)

			n += 1

		with self.query_lock:
			self.found_ip = ip

		self.report({"DEBUG"}, "Query Thread terminating..")
		return

	# -----------  INVOKE  -----------
		
	def find_armature(self, context):
		if not context.scene.chordata.target:
			context.scene.chordata.target = u.get_chordata_armature()

		if not context.scene.chordata.target:
			self.report({"ERROR"},"No Chordata Armature in the scene")
			return False

		return True


	# -----------  EXECUTE  -----------
		
	def execute(self, context):
		self.chord_global = context.user_preferences.addons[__package__].preferences
		
		if not self.find_armature(context):
			return {"CANCELLED"}

		self.chord_global.state = u.possible_states()[0][0]
		self.chord_global.notochord_ip = ""
		self.report({"INFO"},"ip query ongoing..")
		
		wm = context.window_manager
		self._timer = wm.event_timer_add( 0.8 , context.window)
		wm.modal_handler_add(self)

		self.query_lock = threading.Lock()
		self.found_ip = False
		self.query_thread = threading.Thread(target=self.do_query, 
			name="mdns_query")
		self.query_thread.start()

		return {'RUNNING_MODAL'}

	# -----------  CANCEL  -----------

	def cancel(self, context):
		wm = context.window_manager
		wm.event_timer_remove(self._timer)

		print("waiting thread..")
		self.query_thread.join()
		print("thread joined..")

# ======  End of OPERATOR: GET THE NOTOCHORD IP   =======

# ==============================================
# =           OPERATOR: GET LOCAL IP           =
# ==============================================
class Chord_Local_Ip(bpy.types.Operator):
	bl_idname = "chordata.get_local_ip"
	bl_label = "Chordata: get local ip"
	bl_options = {'REGISTER'}

	@classmethod
	def poll(cls, context):
		chord_global = context.user_preferences.addons[__package__].preferences
		return chord_global.trans_method is not "multi"

	def execute(self, context):
		chord_global = context.user_preferences.addons[__package__].preferences
		chord_global.ip_addr = u.get_local_ip()
		return {"FINISHED"}
		


# ======  End of OPERATOR: GET LOCAL IP  =======
