# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018 Bruno Laurencich
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client.  If not, see <https://www.gnu.org/licenses/>.
#
#
# This program uses code from various sources, the default license is GNU GPLv3
# for all code, the dependencies where originally distributed as follows:
# -dnspython library: Copyright 2003-2007, 2009, 2011 Nominum, Inc. Under ISC License
# -osc4py3 library: Copyright 2013-2018 CNRS. Under CeCILL-2.1 license
# -request library: Copyright: 2017 Kenneth Reitz. Under Apache 2.0 License 
#

import bpy
import threading
import time, threading
from re import search as regex
from mathutils import *
from math import *


D = bpy.data
C = bpy.context

class Error(Exception):
	def __init__(self, msg):
		self.msg = msg

def find_root_bone(bones):
	root = None
	for b in bones:
		if not b.parent:
			if root:
				raise Error("more than one Root bone found.")

			root = b

	if root:
		return root

	raise Error("No Root bone found.")

def find_lowest_bone(bones):
	lowest = 0
	for b in bones:
		lowest = min(lowest, b.matrix_local.to_translation().z)
	return lowest


class Armature_Handler:
	#TODO: create a global control for the visualization handling (hiding helpers or mesh)
	def __init__(self, armature_object, helpers):
		if not armature_object or armature_object.type != "ARMATURE":
			raise Error("There's no Armature in the scene.")

		#The armature object
		self.object = armature_object
		#The children meshes (most of the time the ones deformed by the armature)
		self.meshes = [ch for ch in armature_object.children if ch.type== "MESH"]
		#Helpers are objects in the scene that are used to visualize the raw attitude of the K-Ceptors
		if helpers:
			self.helpers = {ob.name: ob for ob in helpers.children }
		#Blender bones, these two collections refer to the same bones, but contain different information (edit mode - pose mode)	
		self.pose = self.object.pose.bones
		self.bones = self.object.data.bones
		#A dict to store our own collection of bones
		self.arm_repr = {}
		#Find the data.(root_bone), and extract the starting local location
		self.root_bone = find_root_bone(self.bones)
		self.root_starting_loc = self.root_bone.matrix_local.to_translation()
		#Find the lowest starting bone, and the relative height of the root at startup
		self.lowest_starting_z = find_lowest_bone(self.bones)
		self.relative_root_height = self.root_starting_loc.z - self.lowest_starting_z	
		#Keep the pose.(root_bone) for later use
		self.root_bone = self.pose[self.root_bone.name]

		print("Root bone: {} | starting location: {}".format(self.root_bone.name, self.root_starting_loc))
		print("Bones found in armature")	
		for b in self.bones:
			if not b.capture_bone:
				continue
			
			b.use_inherit_rotation = False
			has_helper = ""
			try:
				self.helpers[b.name].rotation_mode = "QUATERNION"
				self.helpers[b.name].hide = False
				has_helper = "Helper found"
			except Exception:
				pass

			self.arm_repr[b.name] = {}
			self.arm_repr[b.name]["chord_quat"] = Quaternion()
			self.arm_repr[b.name]["avg_quat"] = Quaternion()
			self.arm_repr[b.name]["diff_quat"] = False
			self.arm_repr[b.name]["local_q"] = b.matrix_local.to_quaternion()

			print(" [{:_<10}] {}".format(b.name, has_helper))

		for m in self.meshes: m.hide = True

		#the message is dislayed on the Blender GUI
		self.message = " Chordata operator | Calibrating... Press ENTER to start posing "

	


	def __del__(self):
		for m in self.meshes: m.hide = False
		try:
			for key,h in self.helpers.items(): h.hide = True
		except Exception:
				pass


	def set_key(self, context):
		for b in self.object.pose.bones:
				self.object.keyframe_insert(\
					'pose.bones["'+ b.name +'"].rotation_quaternion',
						index=-1, 
						frame=context.scene.frame_current, 
						group=b.name)

	def reset_pose(self):
		for b in self.object.pose.bones:
			b.rotation_quaternion.identity()


	def put_quad_on_bones(self, bone_name, quat):
		try:
			self.helpers[bone_name].rotation_quaternion = quat
		except Exception:
				pass

		b = self.arm_repr[bone_name]

		if not b["diff_quat"]:
			#while on calibration mode we just average the quaternions
			b["avg_quat"] = quat.slerp(b["avg_quat"], 0.5)

		else:
			#When in capture mode
			#we set the rotation of the bone to the received sensor rotation
			#taking into account the difference btw them measured during calibration
			q = b["local_q"].conjugated() * quat.copy() * b["local_q"]
			self.object.pose.bones[bone_name].rotation_quaternion = q * b["diff_quat"].conjugated()



	def get_rot_diff(self):
		for m in self.meshes: m.hide = False
		for key, b in self.arm_repr.items():
			try:
				self.helpers[key].hide = True
			except Exception:
				pass

			q0 = b["local_q"].conjugated() * b['avg_quat'].copy() * b["local_q"]
			b["diff_quat"] = self.object.pose.bones[key].rotation_quaternion.rotation_difference(q0)

		self.message = " Chordata operator | Posing... Press ESC to end "
