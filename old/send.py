# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018 Bruno Laurencich
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client.  If not, see <https://www.gnu.org/licenses/>.
#
#
# This program uses code from various sources, the default license is GNU GPLv3
# for all code, the dependencies where originally distributed as follows:
# -dnspython library: Copyright 2003-2007, 2009, 2011 Nominum, Inc. Under ISC License
# -osc4py3 library: Copyright 2013-2018 CNRS. Under CeCILL-2.1 license
# -request library: Copyright: 2017 Kenneth Reitz. Under Apache 2.0 License 
#

import bpy
from mathutils import Vector


D=bpy.data
C=bpy.context


# Import needed modules from osc4py3
from sys import path
from os.path import join, abspath, dirname
path.append(join(dirname(abspath(__file__)), "osc4py3-fork"))
from osc4py3.as_comthreads import *
from osc4py3 import oscbuildparse
from osc4py3 import oscchannel as osch
import time

if "u" in locals():
    import imp
    imp.reload(u)
    imp.reload(arm)
else:
    from . import utils as u
    from . import armature as arm

class Chord_Play_Animation(bpy.types.Operator):
    """Plays and transmits the recorded animation"""
    bl_idname = "chordata.play"
    bl_label = "Chordata: Play and transmit capture"
    bl_options = {'REGISTER'}

    @classmethod
    def poll(cls, context):
        chord_global = context.user_preferences.addons[__package__].preferences
        return context.scene.chordata.target != None \
                and chord_global.receiving == False \
                and context.scene.chordata.target.animation_data \
                and context.scene.chordata.target.animation_data.action

    # -----------  EXECUTE  -----------

    def execute(self, context):
        chord_scene = context.scene.chordata
        self.armature = arm.Armature_Handler(chord_scene.target, chord_scene.helpers)
        # self.armature = context.scene.chordata.target
        self.chord_global = context.user_preferences.addons[__package__].preferences
        self.chord_global.playing = True
        wm = context.window_manager
        self._timer = wm.event_timer_add( \
                            1 / context.scene.render.fps,\
                            context.window)
        wm.modal_handler_add(self)

        curr = context.scene.frame_current
        self.begin, self.end = self.armature.object.animation_data.action.frame_range
        if curr < self.begin or curr > self.end: 
            context.scene.frame_current = self.begin

        self.chord_global.send.net_submask = self.chord_global.net_submask 
        init_osc(self.chord_global.send)

        self.report({"INFO"}, "Capture playing..")
        return {'RUNNING_MODAL'}

    # -----------  MODAL  -----------

    def modal(self, context, event):
        if event.type == 'TIMER':
            send_Armature(self.armature, self.chord_global.send)
            osc_process()
            context.scene.frame_current += 1
            if context.scene.frame_current > self.end: 
                context.scene.frame_current = self.begin

        elif event.type in {'RIGHTMOUSE', 'ESC'} or not self.chord_global.playing:
            self.report({"WARNING"}, "Capture reproduction stopped")
            self.cancel(context)
            return {'CANCELLED'}


        return {'PASS_THROUGH'}

     # -----------  CANCEL  -----------

    def cancel(self, context):
        wm = context.window_manager
        wm.event_timer_remove(self._timer)
        clean()
        self.chord_global.playing = False
        osc_terminate()


class Chord_Stop_Animation(bpy.types.Operator):
    """Stops the reproduction of the animation"""
    bl_idname = "chordata.stop"
    bl_label = "Chordata: Stops capture reproduction and transmition"
    bl_options = {'REGISTER'}


    def execute(self, context):
        self.chord_global = context.user_preferences.addons[__package__].preferences
        self.chord_global.playing = False
        return {"FINISHED"}

class Chord_Toggle_Bones_Send(bpy.types.Operator):
    """Stops the reproduction of the animation"""
    bl_idname = "chordata.toggle_trans_bones"
    bl_label = "Chordata: Set or clear the transmission property of all the bones"
    bl_options = {'REGISTER'}

    clear = bpy.props.BoolProperty( default = False )
    
    @classmethod
    def poll(cls, context):
        chord_global = context.user_preferences.addons[__package__].preferences
        return context.scene.chordata.target != None 

    def execute(self, context):
        arm = context.scene.chordata.target

        for b in arm.pose.bones:
            b.transmit_bone = self.clear

        return {"FINISHED"}

INIT = False

def init_osc(options):
    global INIT
    method = options.trans_method
    addr = u.get_transmition_addr(options)
    port =  options.dest_port

    # Start the system.
    try:
        osc_startup()
    except:
        print("OSC already initialized")

    try:
        # import pdb; pdb.set_trace()
        client = u.transmition_methods(False)[method][3]
        # Make client channels to send packets.
        client(addr, port, "chordata_client")
        INIT = True
        msg = "Transmiting to %s:%d (%s)" % \
            (addr, port, u.transmition_methods(False)[method][0])
        print(msg)
        try:
            u.write_blender_console(bpy.context, msg)
        except Exception as e:
            print("Couldn't write to internal console..")
    except KeyError as e:

        print("Error when trasmitting OSC", e)


last_positions = {}

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def do_transform_axis(q, M):
    #ATTENTION!
    #this was suposed to be an arbitrary matrix, useful for rotations or axis flipping
    #BUT! at the moment is only used for rotations.
    q.rotate(M)

def send_Armature(arm_m, options):
    #TODO: this works, but the implementation could be much more elegant.. (and performative)
    global INIT
    if not INIT: return
    global last_positions
    msgs = []
    chord_scene = bpy.context.scene.chordata


    arm = arm_m.object
    root_bone = arm_m.root_bone
    root_starting_loc = arm_m.root_starting_loc
    relative_root_height = arm_m.relative_root_height

    lowest_bone_z = 0    
    
    for b in arm.pose.bones:
        #if running headless update the scene manually
        if bpy.app.background and b.name == root_bone.name:
            bpy.context.scene.update() 

        if not b.transmit_bone: continue

        try:
            if not arm_m.arm_repr[b.name]["diff_quat"]:
                q = arm_m.arm_repr[b.name]["avg_quat"]
                msgs.append(oscbuildparse.OSCMessage("/Chordata/posecalib/"+b.name, 
                    None, [q[0], q[1], q[2], q[3]]))
                options.send_root_z = False
                continue
        except KeyError as e:
            continue

        if options.send_root_z and root_bone:
            lowest_bone_z = min(b.tail.z, lowest_bone_z)

        if options.send_rot:

            if not options.relative_rotations or not b.parent:
                q = b.matrix.to_quaternion()
            else:
                q = b.parent.matrix.to_quaternion().rotation_difference(b.matrix.to_quaternion()) 

            if options.b_transform_axis and b.name == root_bone.name :
                do_transform_axis( q, options.transform_matrix )

            msgs.append(oscbuildparse.OSCMessage("/Chordata/rotation/"+b.name, 
                None, [q[0], q[1], q[2], q[3]]))
        
        if options.send_pos:
            pos = b.tail
            msgs.append(oscbuildparse.OSCMessage("/Chordata/position/"+b.name, 
                None, [pos[0], pos[1], pos[2]]))

        if options.send_vel:
            if b.name in last_positions.keys():
                print(b.tail, last_positions[b.name])
                vel = b.tail - last_positions[b.name]
                msgs.append(oscbuildparse.OSCMessage("/Chordata/velocity/"+b.name, 
                None, [vel[0], vel[1], vel[2]]))
            
            last_positions[b.name] = b.tail.copy()

        if options.send_dist:
            w_tail = arm.location + b.tail
            if chord_scene.d1:
                dist = w_tail - chord_scene.d1.location
                msgs.append(oscbuildparse.OSCMessage("/Chordata/dist_1/"+b.name,
                    None, [dist.length]))
            if chord_scene.d2:
                dist = w_tail - chord_scene.d2.location
                msgs.append(oscbuildparse.OSCMessage("/Chordata/dist_2/"+b.name,
                    None, [dist.length]))
            if chord_scene.d3:
                dist = w_tail - chord_scene.d3.location
                msgs.append(oscbuildparse.OSCMessage("/Chordata/dist_3/"+b.name,
                    None, [dist.length]))

    if options.send_root_z and root_bone:
        if relative_root_height != 0:
            root_normalized_z = (root_starting_loc.z - lowest_bone_z) / relative_root_height
        else:
            root_normalized_z = root_starting_loc.z - lowest_bone_z

        msgs.append(oscbuildparse.OSCMessage("/Chordata/root_z", None, [root_normalized_z]))

    if not msgs: return

    chord_global = bpy.context.user_preferences.addons[__package__].preferences
    if chord_global.send.split_msg:
        for ch in chunks(msgs, chord_global.send.chunks_n):
            bun = oscbuildparse.OSCBundle(oscbuildparse.OSC_IMMEDIATELY, ch)
            osc_send(bun, "chordata_client")
    else:
        bun = oscbuildparse.OSCBundle(oscbuildparse.OSC_IMMEDIATELY, msgs)
        osc_send(bun, "chordata_client")


def clean():
    global INIT
    if not INIT: return
    global last_positions
    last_positions = {}
    osch.get_channel("chordata_client").terminate()
    INIT = False

