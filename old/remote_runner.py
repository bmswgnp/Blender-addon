# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018 Bruno Laurencich
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client.  If not, see <https://www.gnu.org/licenses/>.
#
#
# This program uses code from various sources, the default license is GNU GPLv3
# for all code, the dependencies where originally distributed as follows:
# -dnspython library: Copyright 2003-2007, 2009, 2011 Nominum, Inc. Under ISC License
# -osc4py3 library: Copyright 2013-2018 CNRS. Under CeCILL-2.1 license
# -request library: Copyright: 2017 Kenneth Reitz. Under Apache 2.0 License 
#

import bpy
from time import time
from sys import path
from os.path import join, abspath, dirname
path.append(join(dirname(abspath(__file__)), "requests"))
from requests import get as http_get
from requests.exceptions import RequestException as Http_Error

if "u" in locals():
	import imp
	imp.reload(u)
else:
	from . import utils as u


#TODO:
# Prepara pr EXEC_DEFAULT contest (check inside execute if invoke was called)
# put connection states inside utils
# Create receive op that starts, receives and stops on exit (perhaps also takes mouse input) 

# =====================================================
# =         OPERATOR: START NOTOCHORD                 =
# =====================================================

def popup_message(self, context):
	self.layout.label("Notochord message.")

class Chord_Runner_Base(bpy.types.Operator):
	bl_idname = "chordata.runner_base"
	bl_label = "Chordata: runner_base"
	bl_options = {'INTERNAL'}

	m_no_ip = """Imposible to resolve notochord address, please add the notochord's ip address manually """
	m_not_connected = """Notochord is not connected"""
	m_error = """There was an error with the notochord program"""
	
	def disconnected(self):
		self.report({"WARNING"},self.m_not_connected)
		bpy.context.window_manager.popup_menu( popup_message, 
					title=self.m_not_connected, icon='ERROR')
		self.chord_global.state = "Disconnected"
		return {"CANCELLED"}

	def remote_error(self):
		self.report({"WARNING"},self.m_error)
		bpy.context.window_manager.popup_menu( popup_message, 
					title=self.m_error, icon='ERROR')
		return {"CANCELLED"}


class Chord_Start_Notochord(Chord_Runner_Base):
	"""Starts the notochord program"""
	bl_idname = "chordata.start_notochord"
	bl_label = "Chordata: Start the notochord"
	bl_options = {'REGISTER'}

	def draw(self, context):

		self.layout.separator()
		self.layout.label("Press OK to start the notochord, might take some seconds")

	def check_local_ip(self):
		if self.chord_global.ip_addr:
			return True

		bpy.ops.chordata.get_local_ip()

		if self.chord_global.ip_addr:
			return True

		return False


	def invoke(self, context, event):
		self.chord_global = context.user_preferences.addons[__package__].preferences
		if not self.check_local_ip():
			self.report({"WARNING"}, self.m_no_ip)
			bpy.context.window_manager.popup_menu(popup_message, 
							title=self.m_no_ip, icon='ERROR')
			return {"CANCELLED"}

		try:
			params = "?"
			for p in self.chord_global.notochord_parameters.split():
				params += "p=%s&" % p

			addr = u.get_transmition_addr(self.chord_global)

			params += "p=%s&p=%s" % (addr, self.chord_global.notochord_dest_port)

			r = http_get("http://%s/command/init%s" % \
				(self.chord_global.notochord_ip, params),\
				timeout=1)

		except Http_Error:
			u.write_blender_console(context, str(Http_Error))
			return self.disconnected()

		if r.status_code not in [200, 202]:
			u.write_blender_console(context, "Status Code: %d" % r.status_code)
			return self.disconnected()

		notochord_response = r.json()

		u.write_blender_console(context, notochord_response["msg"])

		if notochord_response["mode"] != "normal":
			return self.remote_error()

		return context.window_manager.invoke_props_dialog(self)

	# -----------  EXECUTE  -----------
		
	def execute(self, context):
		self.chord_global = context.user_preferences.addons[__package__].preferences
		self.report({"INFO"},"Starting notochord..")
		
		try:
			r = http_get("http://%s/command/send" % self.chord_global.notochord_ip, timeout=1)

		except Http_Error:
			u.write_blender_console(context, str(Http_Error))
			return self.disconnected()

		if r.status_code not in [200, 202]:
			u.write_blender_console(context, "Status Code: %d" % r.status_code)
			return self.disconnected()

		notochord_response = r.json()

		if notochord_response["mode"] != "normal":
			u.write_blender_console(context, "Running mode: " + notochord_response["mode"])
			return self.remote_error()

		self.report({"INFO"},"Transfering pose data..")
		self.chord_global.state = "Transfering"
		
		return {'FINISHED'}


# ======  End of OPERATOR: START NOTOCHORD   =======

# =================================================
# =           OPERATOR: CLOSE NOTOCHORD           =
# =================================================

class Chord_Close_Notochord(Chord_Runner_Base):
	"""Starts the notochord program"""
	bl_idname = "chordata.close_notochord"
	bl_label = "Chordata: Close the notochord"
	bl_options = {'REGISTER'}

	def draw(self, context):

		self.layout.separator()
		self.layout.label("Stop the notochord?")

	def invoke(self, context, event):
		return context.window_manager.invoke_props_dialog(self)

	# -----------  EXECUTE  -----------

	def execute(self, context):
		self.chord_global = context.user_preferences.addons[__package__].preferences
		self.report({"INFO"},"Closing notochord..")
		
		try:
			r = http_get("http://%s/command/close" % self.chord_global.notochord_ip, timeout=1)

		except Http_Error:
			return self.disconnected()

		if r.status_code != 200:
			return self.disconnected()

		notochord_response = r.json()

		if notochord_response["state"] != 0:
			return self.remote_error()

		self.chord_global.state = "Connected"
		
		u.write_blender_console(context, notochord_response["msg"])

		return {'FINISHED'}

# ======  End of OPERATOR: CLOSE NOTOCHORD  =======
