# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018 Bruno Laurencich
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client.  If not, see <https://www.gnu.org/licenses/>.
#
#
# This program uses code from various sources, the default license is GNU GPLv3
# for all code, the dependencies where originally distributed as follows:
# -dnspython library: Copyright 2003-2007, 2009, 2011 Nominum, Inc. Under ISC License
# -osc4py3 library: Copyright 2013-2018 CNRS. Under CeCILL-2.1 license
# -request library: Copyright: 2017 Kenneth Reitz. Under Apache 2.0 License 
#

import bpy
from time import time
from sys import path
from os.path import join, abspath, dirname
path.append(join(dirname(abspath(__file__)), "requests"))
from requests import get as http_get
from requests.exceptions import RequestException as Http_Error


CHECK_PERIOD = 5

def get_conn_state_attr(context, attr = "name"):
	current = context.user_preferences.addons[__package__].preferences.state
	return getattr(bpy.types.Chord_Properties\
			.bl_rna.properties['state']\
			.enum_items[current], attr )


#
#    Menu in tools region, Chordata tab
#
class Chord_tools_panel(bpy.types.Panel):
	"""Main panel for the chordata client addon"""
	bl_label = "Chordata"
	bl_category = "Chordata"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"
 
	def check_connection(self):
		try:
			r = http_get("http://%s/watchdog" % self.chord_global.notochord_ip, timeout=1)
		except Http_Error:
			# self.report({"WARNING"},"Notochord disconnected")
			self.chord_global.state = "Disconnected"
			return

		if r.status_code != 200:
			# self.report({"WARNING"},"Notochord disconnected")
			self.chord_global.state = "Disconnected"
		# else:
		# 	self.report({"INFO"},"Notochord still there")
			

	def draw(self, context):
		self.chord_global = context.user_preferences.addons[__package__].preferences
		layout = self.layout
		#Watchdog removed momentarily: might incur on unnecesary overhead
		# if time() - self.chord_global.last_check > CHECK_PERIOD:
		# 	self.chord_global.last_check = time()
		# 	if self.chord_global.state == "Connected":
		# 		self.check_connection()
		# if get_conn_state_attr(context, "value") > 2:
		# 	layout.enabled= False

		layout.label("Armature Object")
		row = layout.row(align=True)
		target = context.scene.chordata.target 
		if not target or target.type != "ARMATURE":
			row.alert = True
		row.prop(context.scene.chordata, "target", text="", icon="MOD_ARMATURE")

		layout.separator()
		row = layout.row()
		row.scale_y = 2
		row.operator("chordata.get_ip", "Connect")
		layout.label(self.chord_global.state,\
					 icon= get_conn_state_attr(context, "icon"))
		
		layout.prop(self.chord_global, "manage_notochord", expand=True)

		recv_icon = "POSE_HLT", "ARMATURE_DATA"
		if self.chord_global.manage_notochord: recv_icon = "RADIO", "CANCEL"
		
		row = layout.row(align=True)
		row.operator("chordata.receive", "Receive", icon=recv_icon[0])
		row.operator("chordata.stop_receive", "Stop", icon=recv_icon[1])  

		
		### BOX
		layout.separator()
		box = layout.box()
		box.label("Notochord Transmision method", icon="NLA_PUSHDOWN")
		row = box.row()
		row.prop(self.chord_global, "trans_method", expand=True)

		row = box.row()
		split = row.split()#percentage=0.25
		col = split.column()
		# col.operator("my.button", text="21").loc="5 21"

		if self.chord_global.trans_method == "multi":
			col.label("multi_id group")
			col.prop(self.chord_global, "multicast_grp_addr", text="")		
		else:
			col.label("Local address")
			col.prop(self.chord_global, "ip_addr", text="")
		split = row.split()#percentage=0.25
		col = split.column()
				
		col.label("Port")
		col.prop(self.chord_global, "notochord_dest_port", text="")
		if self.chord_global.trans_method == "broad":
			row = box.row()
			row.prop(self.chord_global, "net_submask", text="Net submask")

		layout.prop(self.chord_global, "show_rec", icon="REC")
		layout.prop(self.chord_global, "show_send", icon="MAN_TRANS")
		layout.prop(self.chord_global, "show_advanced", icon="SETTINGS")


class Chord_rec_panel(bpy.types.Panel):
	"""Recording options panel for the chordata client addon"""
	bl_label = "Chordata Record/Play"
	bl_category = "Chordata"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"

	@classmethod
	def poll(cls, context):
		chord_global = context.user_preferences.addons[__package__].preferences
		return chord_global.show_rec

	def draw(self, context):
		self.chord_global = context.user_preferences.addons[__package__].preferences
		layout = self.layout
		if context.scene.chordata.target == None:
			layout.active = False
		row = layout.row()
		row.scale_y = 2
		row.prop(self.chord_global, "do_set_keyframes", icon="REC")
		layout.operator("chordata.clean_anims", "Clean Animation", icon="GHOST")

		row = layout.row(align=True)
		row.alignment = 'EXPAND'
		if not self.chord_global.playing:
			row.operator("chordata.play", "Play and trasmit Animation", icon="PLAY")
		else:
			row.operator("chordata.play", "Playing..", icon="PLAY")
			row.operator("chordata.stop", "Stop", icon="CANCEL")




class Chord_send_panel(bpy.types.Panel):
	"""Retransmision options panel for the chordata client addon"""
	bl_label = "Chordata Retransmit"
	bl_category = "Chordata"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"

	@classmethod
	def poll(cls, context):
		chord_global = context.user_preferences.addons[__package__].preferences
		return chord_global.show_send

	def draw(self, context):
		self.chord_global = context.user_preferences.addons[__package__].preferences
		layout = self.layout
		if context.scene.chordata.target == None:
			layout.active = False
		box = layout.box()
		box.label("Retransmision method", icon="MAN_TRANS")
		row = box.row()
		row.prop(self.chord_global.send, "trans_method", expand=True)

		row = box.row()
		split = row.split()#percentage=0.25
		col = split.column()
		# col.operator("my.button", text="21").loc="5 21"

		if self.chord_global.send.trans_method == "none":
			return
		elif self.chord_global.send.trans_method == "multi":
			col.label("multi_id group")
			col.prop(self.chord_global.send, "multicast_grp_addr", text="")		
		else:
			col.label("Destination address")
			col.prop(self.chord_global.send, "ip_addr", text="")
		split = row.split()#percentage=0.25
		col = split.column()
				
		col.label("Port")
		col.prop(self.chord_global.send, "dest_port", text="")
		if self.chord_global.send.trans_method == "broad":
			row = box.row()
			row.prop(self.chord_global, "net_submask", text="Net submask")

		layout.label("Options:")
		layout.prop(self.chord_global.send, "relative_rotations", text="Use relative rotations")
		layout.prop(self.chord_global.send, "send_root_z", text="Send root bone height")

		layout.label("Send properties:")
		layout.prop(self.chord_global.send, "send_rot", text="Rotation")
		layout.prop(self.chord_global.send, "send_pos", text="Position")
		layout.prop(self.chord_global.send, "send_vel", text="Velocity")
		layout.prop(self.chord_global.send, "send_dist", text="Distance")

		if self.chord_global.send.send_dist:
			layout.prop(context.scene.chordata, "d1", text="Target #1")
			layout.prop(context.scene.chordata, "d2", text="Target #2")
			layout.prop(context.scene.chordata, "d3", text="Target #3")

		layout.label("Split output:")
		row = layout.row()
		container = row
		if self.chord_global.send.split_msg:
			container = row.split(percentage=0.6)
		container.prop(self.chord_global.send, "split_msg")
		if self.chord_global.send.split_msg:
			container.prop(self.chord_global.send, "chunks_n", text="max")

		layout.label("Transform quat to..")
		layout.prop(self.chord_global.send, "which_transform", expand=True)





		


class Chord_advanced_panel(bpy.types.Panel):
	"""Advanced options panel for the chordata client addon"""
	bl_label = "Chordata Advanced"
	bl_category = "Chordata"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"

	@classmethod
	def poll(cls, context):
		chord_global = context.user_preferences.addons[__package__].preferences
		return chord_global.show_advanced

	def draw(self, context):
		self.chord_global = context.user_preferences.addons[__package__].preferences
		layout = self.layout

		layout.prop(self.chord_global, "debug_mode", text="Debug mode", icon='LIBRARY_DATA_BROKEN')

		layout.label("Control notochord")
		row = layout.row(align=True)
		row.alignment = 'EXPAND'

		row.operator("chordata.start_notochord", "Start")
		row.operator("chordata.close_notochord", "Stop")
		
		layout.label("Parameters")
		layout.prop(self.chord_global, "notochord_parameters", text="")

		layout.label("Notochord hostname")
		layout.prop(self.chord_global, "notochord_hostname", text="")

		target = context.scene.chordata.target
		

		
		if target and target.mode == "POSE": 
			active_b = target.data.bones.active
			if active_b: 
				box = layout.box()
				box.label("Bone [%s]" % active_b.name)
				box.prop(active_b, "capture_bone", icon="POSE_DATA")
				box.prop(target.pose.bones[active_b.name], "transmit_bone", icon="MAN_TRANS")
				box.label("Set transmit on:")
				row = box.row(align=True)
				row.operator("chordata.toggle_trans_bones", "All bones").clear = True
				row.operator("chordata.toggle_trans_bones", "No bones").clear

		layout.label("Capture OSC pattern")
		layout.prop(self.chord_global, "capture_quat_patt", text= "")

		layout.label("Sensor helpers parent")
		layout.prop(context.scene.chordata, "helpers", text="", icon="OUTLINER_DATA_EMPTY")
		
		

		box = layout.box()
		# box.label("This button will save many Chordata setting, as well as other preferences set in the 'User preferences menu' during the current session")
		box.operator("wm.save_userpref", "Save User_preferences", icon="SAVE_PREFS")
