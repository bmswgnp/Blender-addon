# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018 Bruno Laurencich
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client.  If not, see <https://www.gnu.org/licenses/>.
#
#
# This program uses code from various sources, the default license is GNU GPLv3
# for all code, the dependencies where originally distributed as follows:
# -dnspython library: Copyright 2003-2007, 2009, 2011 Nominum, Inc. Under ISC License
# -osc4py3 library: Copyright 2013-2018 CNRS. Under CeCILL-2.1 license
# -request library: Copyright: 2017 Kenneth Reitz. Under Apache 2.0 License 
#

import bpy
from sys import path
from os.path import join, abspath, dirname
path.append(join(dirname(abspath(__file__)), "osc4py3-fork"))
import osc4py3.as_comthreads
from osc4py3.as_comthreads import *
import ipaddress
import socket

def get_transmition_addr(options):
	# import pdb; pdb.set_trace()
	addr = options.ip_addr
	if options.trans_method == "broad":
		addr = options.ip_addr
		subm = options.net_submask
		addr = get_broadcast_addr(addr, subm)
	elif options.trans_method == "multi":
		addr = options.multicast_grp_addr
	return addr		


def get_local_ip():
	"""Get local ip, from https://stackoverflow.com/a/28950776 """
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	try:
		# doesn't even have to be reachable
		s.connect(('10.255.255.255', 1))
		IP = s.getsockname()[0]
	except:
		IP = ""
	finally:
		s.close()
	return IP

def get_chordata_armature():
	for ob in bpy.data.objects:
		if ob.name.lower() == "chordata":
			if ob.type == "ARMATURE":
				return ob
	return None

def transmition_methods(as_tuples = True):
	"""Transmision method to be used"""
	o = osc4py3.as_comthreads
	d = {
		"uni": ("Unicast", 0, o.osc_udp_server, o.osc_udp_client),
		"multi": ("Multicast", 1, o.osc_multicast_server, o.osc_multicast_client), 
		"broad": ("Broadcast", 2, o.osc_broadcast_server, o.osc_broadcast_client),  
	}

	if as_tuples:
		return [(name, v[0], v[0] + " transmition", "", v[1] ) for name, v in d.items()]

	return d

def sending_props(as_tuples = True):
	"""properties to retransmit"""
	d = {
		"None":("none", 0),  
		"Rotation": ("rot", 1, ),
		"Position": ("pos", 2, ), 
		"Velocity": ("vel", 3, ),
	}

	if as_tuples:
		return [(name, v[0], name + "_lala", "", v[1] ) for name, v in d.items()]

	return d


def get_broadcast_addr(addr, submask):
	return str(ipaddress.IPv4Network( "%s/%s" % (addr, submask), False).broadcast_address)


def possible_states():
	"""Returns the 'items' to create the states enum property. 
	The format is a list of tuples like:
	[(identifier, name, description, icon, number), ...]
	complete doc in:
	https://docs.blender.org/api/blender_python_api_2_77_0/bpy.props.html#bpy.props.EnumProperty """
	return [
			("Disconnected", "no-conn", "", "COLOR_RED", 0),
			("Connected", "conn", "", "COLOR_GREEN", 1),
			("Transfering", "transfer", "", "POSE_DATA", 2),
			("Searching..", "search1", "", "INLINK", 3),
			("Searching.", "search2", "", "LINK", 4),
			]


def write_blender_console(context, t):
	running_headless = bpy.app.background
	screen = context.screen

	for area in screen.areas:
		if area.type == 'CONSOLE':
			for r in area.regions:
				if r.type == "WINDOW":
					override = {'window': context.window, 'screen': screen, 
								'area': area, "region": r}
					for line in t.split("\r\n"):
						if running_headless:
							print("||", line, "||")
						else:
							bpy.ops.console.scrollback_append(override, text=line)
					break