import bpy.props
import bpy.types
from bpy.types import Node, NodeSocket
from ..chordatatree import ChordataTreeNode
from bpy.app import timers

import requests
import json
from . import colors

from ..osc4py3 import as_eventloop as osc_as_eventloop
from ..osc4py3 import oscmethod


def timed_osc_process():
    osc_as_eventloop.osc_process()
    # this would stop the timer
    # return None
    # instead we ask to repeat this function in 0.1s
    return 0.1


def osc_handler(arg, extra):
    print(arg, extra['tree'], extra['node'])
    bpy.data.node_groups[extra['tree']].nodes[extra['node']].osc_msg = arg


class StartNotochordOperator(bpy.types.Operator):
    bl_idname = "chordata.notochord_start"
    bl_label = "Start Notochord Connection"

    receiver_port: bpy.props.IntProperty()
    notochord_address: bpy.props.StringProperty()
    node_path: bpy.props.StringProperty()
    tree_path: bpy.props.StringProperty()

    def execute(self, context):
        #print(self.tree_path, self.node_path)
        url = "http://" + self.notochord_address
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        data = {
            'port': self.receiver_port
        }
        r = requests.post(url, data=json.dumps(data), headers=headers)
        # TO-DO: actually check if response was successfull
        timers.register(timed_osc_process)
        osc_as_eventloop.osc_startup()
        osc_as_eventloop.osc_udp_server(
            "127.0.0.1", self.receiver_port, "chordata_osc_server")
        osc_as_eventloop.osc_method(
            "/chan1*", osc_handler, argscheme=oscmethod.OSCARG_DATAUNPACK + oscmethod.OSCARG_EXTRA, extra=dict(node=self.node_path, tree=self.tree_path))
        return {'FINISHED'}


class StopNotochordOperator(bpy.types.Operator):
    bl_idname = "chordata.notochord_stop"
    bl_label = "Stop Notochord Connection"

    def execute(self, context):
        timers.unregister(timed_osc_process)
        osc_as_eventloop.osc_terminate()
        return {'FINISHED'}


class OscInNode(Node, ChordataTreeNode):
    # === Basics ===
    # Description string
    '''Receive OSC test node'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'OscInNodeType'
    # Label for nice name display
    bl_label = "Osc In Node"
    # Icon identifier
    bl_icon = 'SOUND'

    notochord_address: bpy.props.StringProperty(default="127.0.0.1:9000")

    receiver_port: bpy.props.IntProperty(
        default=7000
    )

    osc_msg: bpy.props.FloatProperty(default=0.0)

    def init(self, context):
        self.width = 525.0

    def draw_buttons(self, context, layout):
        layout.prop(self, "receiver_port", text="Receiver Port")
        layout.prop(self, "notochord_address", text="Notochord Address")
        layout.label(text="OSC MSG: " + str(self.osc_msg))
        row = layout.row(align=True)
        # https://blenderartists.org/t/custom-operator-with-arguments/550526
        # start notochord button
        start_notochord_op = row.operator("chordata.notochord_start",
                                          text="Connect")
        start_notochord_op.notochord_address = self.notochord_address
        start_notochord_op.receiver_port = self.receiver_port
        start_notochord_op.tree_path = self.id_data.name
        start_notochord_op.node_path = self.name
        # stop notochord button
        row.operator("chordata.notochord_stop", text="Disconnect")

    # Optional: custom label
    # Explicit user label overrides this, but here we can define a label dynamically

    def draw_label(self):
        return "Osc In Node"


to_register = (StartNotochordOperator, StopNotochordOperator, OscInNode)
