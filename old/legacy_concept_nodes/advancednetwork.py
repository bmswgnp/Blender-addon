import bpy.props
from bpy.types import Node, NodeSocket
from ..chordatatree import ChordataTreeNode

from . import colors


def on_ip_update(self, context):
    for link in self.outputs["Network"].links:
        link.to_node.ip = self.ip


def on_port_update(self, context):
    for link in self.outputs["Network"].links:
        link.to_node.port = self.port


def on_mode_update(self, context):
    for link in self.outputs["Network"].links:
        link.to_node.transmission_mode = self.transmission_mode


class AdvancedNetworkOutputSocket(NodeSocket):
    # Description string
    '''Custom output float node socket type'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'AdvancedNetworkOutputSocketType'
    # Label for nice name display
    bl_label = "Chordata Node Ip Socket"

    # Optional function for drawing the socket input value

    def draw(self, context, layout, node, text):
        if not self.is_linked:
            layout.label(text="Advanced Network")
        else:
            layout.label(text="Advanced Network")

    # Socket color
    def draw_color(self, context, node):
        return colors.main


class AdvancedNetworkNode(Node, ChordataTreeNode):
    # === Basics ===
    # Description string
    '''Chordata motion capture system main node'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'AdvancedNetworkNodeType'
    # Label for nice name display
    bl_label = "Advanced Network Node"
    # Icon identifier
    bl_icon = 'SOUND'

    ip: bpy.props.StringProperty(
        default="127.0.0.1", update=on_ip_update)

    port: bpy.props.IntProperty(
        default=7000, update=on_port_update
    )

    transmission_mode: bpy.props.EnumProperty(
        name="Transmission mode",
        description="Network transmission mode used",
        items=[
            ("UNICAST", "Unicast", "For notochord Unicast transmission"),
            ("BROADCAST", "Broadcast", "For notochord Broadcast transmission"),
            ("MULTICAST", "Multicast", "For notochord Multicast transmission")
        ],
        update=on_mode_update
    )

    def init(self, context):
        self.outputs.new('AdvancedNetworkOutputSocketType', "Network")
        self.width = 350.0

    def draw_buttons(self, context, layout):
        layout.prop(self, "transmission_mode", expand=True)
        layout.prop(self, "ip", text="IP")
        layout.prop(self, "port", text="Port")

    # Optional: custom label
    # Explicit user label overrides this, but here we can define a label dynamically

    def draw_label(self):
        return "Advanced Network Node"


to_register = (AdvancedNetworkOutputSocket, AdvancedNetworkNode)
