import bpy.props
from bpy.types import Node, NodeSocket
from ..chordatatree import ChordataTreeNode

from . import colors


def on_ip_update(self, context):
    print("Notochord ip set to: " + self.ip)


def on_port_update(self, context):
    print("Notochord port set to: " + str(self.port))


def on_armature_update(self, context):
    try:
        print("Armature set to " + self.armature.name)
    except:
        print("No armature selected.")


def on_mode_update(self, context):
    print("Transmission mode set to: " + self.transmission_mode)


class AdvancedNetworkInputSocket(NodeSocket):
    # Description string
    '''Custom output float node socket type'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'AdvancedNetworkInputSocketType'
    # Label for nice name display
    bl_label = "Chordata Node Ip Socket"

    # Optional function for drawing the socket input value

    def draw(self, context, layout, node, text):
        layout.label(text="Advanced Network")

    # Socket color
    def draw_color(self, context, node):
        return colors.main


class ChordataMainNode(Node, ChordataTreeNode):
    # === Basics ===
    # Description string
    '''Chordata motion capture system main node'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'ChordataMainNodeType'
    # Label for nice name display
    bl_label = "Chordata Main Node"
    # Icon identifier
    bl_icon = 'SOUND'

    ip: bpy.props.StringProperty(
        default="127.0.0.1", update=on_ip_update)

    port: bpy.props.IntProperty(
        default=7000, update=on_port_update
    )

    transmission_mode: bpy.props.EnumProperty(
        name="Transmission Mode",
        description="Network transmission mode used",
        items=[
            ("UNICAST", "Unicast", "For notochord Unicast transmission"),
            ("BROADCAST", "Broadcast", "For notochord Broadcast transmission"),
            ("MULTICAST", "Multicast", "For notochord Multicast transmission")
        ],
        default="UNICAST",
        update=on_mode_update
    )

    armature: bpy.props.PointerProperty(
        type=bpy.types.Armature, update=on_armature_update)

    def init(self, context):
        self.inputs.new('AdvancedNetworkInputSocketType', "Network")
        self.width = 350.0

    def update(self):
        # validating same socket communication
        # https://blender.stackexchange.com/questions/91439/call-a-function-when-new-node-link-is-created
        # https://blender.stackexchange.com/questions/114618/how-do-nodesockets-determine-if-a-link-between-them-is-possible
        for link in self.inputs["Network"].links:
            if link.from_socket.bl_idname == 'AdvancedNetworkOutputSocketType':
                print(link, " is valid!")
            else:
                print(link, " is NOT valid! Removing it..")
                node_tree = self.id_data
                node_tree.links.remove(link)
        if not self.inputs["Network"].is_linked:
            self.transmission_mode = "UNICAST"

    def draw_buttons(self, context, layout):
        # exposes network properties only is advanced network node is not connected
        layout.prop(self, "armature", text="Armature")
        layout.separator()
        if not self.inputs['Network'].is_linked:
            layout.prop(self, "ip", text="IP")
            layout.prop(self, "port", text="Port")
        row = layout.row(align=True)
        row.operator("render.render", text="Connect")
        row.operator("render.render", text="Disconnect")

    # Detail buttons in the sidebar.
    # If this function is not defined, the draw_buttons function is used instead
    def draw_buttons_ext(self, context, layout):
        # exposes network properties only is advanced network node is not connected
        if not self.inputs["Network"].is_linked:
            layout.prop(self, "ip", text="IP")
            layout.prop(self, "port", text="Port")
        # if advanced network is connected just shows labels
        else:
            layout.label(text="Transmission Mode: " + self.transmission_mode)
            layout.label(text="IP: " + self.ip)
            layout.label(text="Port: " + str(self.port))
        layout.prop(self, "armature", text="Armature")
        row = layout.row(align=True)
        row.operator("render.render", text="Connect")
        row.operator("render.render", text="Disconnect")

    # Optional: custom label
    # Explicit user label overrides this, but here we can define a label dynamically

    def draw_label(self):
        return "Chordata Main Node"


to_register = (AdvancedNetworkInputSocket, ChordataMainNode)
