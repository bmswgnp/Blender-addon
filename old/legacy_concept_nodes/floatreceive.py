import bpy.props
from bpy.types import Node, NodeSocket
from ..chordatatree import ChordataTreeNode

from . import colors

def on_float_value_update(self, context):
    if self.is_output:
        for link in self.links:
            link.to_socket.float_value = self.float_value


class CustomFloatSocket(NodeSocket):
    # Description string
    '''Custom output float node socket type'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'CustomFloatSocketType'
    # Label for nice name display
    bl_label = "Custom Float Node Socket"

    float_value: bpy.props.FloatProperty(
        default=0, update=on_float_value_update)

    # Optional function for drawing the socket input value
    def draw(self, context, layout, node, text):
        if self.is_output:
            layout.prop(self, "float_value", text=text)
        else:
            layout.label(text=str(self.float_value))

    # Socket color
    def draw_color(self, context, node):
        return colors.default


class FloatReceiveNode(Node, ChordataTreeNode):
    # === Basics ===
    # Description string
    '''A custom node'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'FloatReceiveNodeType'
    # Label for nice name display
    bl_label = "Float Receive Node"
    # Icon identifier
    bl_icon = 'SOUND'

    def init(self, context):
        self.inputs.new('CustomFloatSocketType', "Float input")

    # Optional: custom label
    # Explicit user label overrides this, but here we can define a label dynamically
    def draw_label(self):
        return "Receive Float"

to_register = (CustomFloatSocket, FloatReceiveNode)