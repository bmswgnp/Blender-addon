import bpy.props
from bpy.types import Node, NodeSocket
from ..chordatatree import ChordataTreeNode

from . import colors


def on_add_float_value_update(self, context):
    self.node.outputs['Result'].float_value = self.node.inputs["Float A"].float_value + \
        self.node.inputs["Float B"].float_value


def on_float_value_update(self, context):
    if self.is_output:
        for link in self.links:
            link.to_socket.float_value = self.float_value

# Custom socket type


class CustomFloatSocket(NodeSocket):
    # Description string
    '''Custom output float node socket type'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'CustomFloatSocketType'
    # Label for nice name display
    bl_label = "Custom Float Node Socket"

    float_value: bpy.props.FloatProperty(
        default=0, update=on_float_value_update)

    # Optional function for drawing the socket input value
    def draw(self, context, layout, node, text):
        if self.is_output:
            layout.prop(self, "float_value", text=text)
        else:
            layout.label(text=str(self.float_value))

    # Socket color
    def draw_color(self, context, node):
        return colors.default


# Custom Socket for Add Float Node
class AddFloatSocket(NodeSocket):
    # Description string
    '''Float socket for float add node'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'AddFloatSocketType'
    # Label for nice name display
    bl_label = "Add Float Node Socket"

    float_value: bpy.props.FloatProperty(
        default=0, update=on_add_float_value_update)

    def draw(self, context, layout, node, text):
        if not self.is_linked:
            layout.prop(self, "float_value", text=text)
        else:
            layout.label(text=str(self.float_value))

    # Socket color
    def draw_color(self, context, node):
        return (colors.default)


# Derived from the Node base type.
class AddFloatNode(Node, ChordataTreeNode):
    # === Basics ===
    # Description string
    '''Adds floats node'''
    # Optional identifier string. If not explicitly defined, the python class name is used.
    bl_idname = 'AddFloatNodeType'
    # Label for nice name display
    bl_label = "Add Float"
    # Icon identifier
    bl_icon = 'SOUND'

    def init(self, context):
        self.outputs.new('CustomFloatSocketType', "Result")
        self.inputs.new('AddFloatSocketType', "Float A")
        self.inputs.new('AddFloatSocketType', "Float B")

    # Optional: custom label
    # Explicit user label overrides this, but here we can define a label dynamically
    def draw_label(self):
        return "Add Float"


to_register = (AddFloatNode, AddFloatSocket)
