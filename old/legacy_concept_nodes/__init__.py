if "bpy" in locals():
    import imp
    imp.reload(chordatamain)
    imp.reload(floatsend)
    imp.reload(floatreceive)
    imp.reload(floatadd)
    imp.reload(advancednetwork)
    imp.reload(oscin)

else:
    from . import oscin
    from . import chordatamain
    from . import floatsend
    from . import floatreceive
    from . import floatadd
    from . import advancednetwork

import bpy

to_register = chordatamain.to_register \
    + floatsend.to_register \
    + floatreceive.to_register \
    + floatadd.to_register \
    + advancednetwork.to_register \
    + oscin.to_register \



def register():
    from bpy.utils import register_class
    for cls in to_register:
        register_class(cls)


def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(to_register):
        unregister_class(cls)
