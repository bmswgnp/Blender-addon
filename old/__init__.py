# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018 Bruno Laurencich
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client.  If not, see <https://www.gnu.org/licenses/>.
#
#
# This program uses code from various sources, the default license is GNU GPLv3
# for all code, the dependencies where originally distributed as follows:
# -dnspython library: Copyright 2003-2007, 2009, 2011 Nominum, Inc. Under ISC License
# -osc4py3 library: Copyright 2013-2018 CNRS. Under CeCILL-2.1 license
# -request library: Copyright: 2017 Kenneth Reitz. Under Apache 2.0 License 
#


bl_info = {
    "name": "Chordata motion capture client",
    "author": "Bruno Laurencich",
    "blender": (2, 79, 0),
    "version": (0, 1, 1),
    "location": "View 3D > Tools shelf > Chordata tab",
    "description": "A client to manage the physical motion capture performed with the Chordata system",
    "warning": "",
    "wiki_url": "http://chordata.cc",
    "tracker_url": "https://gitlab.com/chordata/Blender-addon/issues",
    "support": "COMMUNITY",
    "category": "Rigging",
}

# To support reload properly, try to access a package var, 
# if it's there, reload everything
if "bpy" in locals():
	import imp
	imp.reload(connection)
	imp.reload(panel)
	imp.reload(remote_runner)
	imp.reload(u)
	imp.reload(receive)
else:
	from . import connection
	from . import panel
	from . import remote_runner
	from . import utils as u
	from . import receive
	from sys import path
	from os.path import join, abspath, dirname
	path.append(join(dirname(abspath(__file__)), "osc4py3-fork"))


import bpy
from bpy import props
from bpy.types import WindowManager
from mathutils import Matrix, Euler
from math import pi
import faulthandler
C = bpy.context

# =========================================
# =           GLOBAL PROPERTIES           =
# =========================================

def check_local_ip(self, context):
	chord_global = context.user_preferences.addons[__package__].preferences
	if chord_global.trans_method != "multi":
		chord_global.ip_addr = u.get_local_ip()


class Chord_Properties(bpy.types.AddonPreferences):
	"""A collection to hold all the global chordata properties.
	can be retrived at: 
	bpy.context.user_preferences.addons['<addon_name>'].preferences"""

	bl_idname = __package__

	state = props.EnumProperty(\
		items=u.possible_states(),\
		options={'SKIP_SAVE'}
		)

	trans_method = props.EnumProperty(\
		items= u.transmition_methods(), 
		default="uni",
		update=check_local_ip)

	net_submask = props.StringProperty( default = "255.255.255.0" )

	notochord_msg = props.StringProperty( default = "" )

	multicast_grp_addr = props.StringProperty( default = "239.0.0.1" )
	
	ip_addr = props.StringProperty( default = u.get_local_ip() )

	notochord_dest_port = props.IntProperty( default = 6565 )

	notochord_ip = props.StringProperty( default = "" )

	manage_notochord = props.BoolProperty( name="Manage notochord",
		description="Start the notochord when receiving, and finish it on close",  default = False )

	show_advanced = props.BoolProperty( name="Show advanced panel",  default = False )
	show_send = props.BoolProperty( name="Show retransmision panel",  default = False )
	show_rec = props.BoolProperty( name="Show recording panel",  default = False )

	# -----------  RECORD  -----------
	
	do_set_keyframes = props.BoolProperty( name="Record capture",  default = False )

	playing = props.BoolProperty( name="Record capture",  default = False )

	# -----------  ADVANCED  -----------
	def on_debug_update(self, context):
		if self.debug_mode:
			faulthandler.enable()
			print("*** Chordata Debug mode enable ***")
		else:
			faulthandler.disable()
			print("** Chordata Debug mode disable **")

	debug_mode = props.BoolProperty( name="Enable debuging for the Chordata add-on",  default = False, update=on_debug_update )
	
	notochord_parameters = props.StringProperty( default = "" )

	notochord_hostname = props.StringProperty( name="Notochord hostname", 
		description="The hostname to be used for the ip resolution", default = "notochord" )

	capture_quat_patt = props.StringProperty( name = "Capture OSC pattern", 
	description = "Regex pattern to filter bone quaternion data only",  default = r"^/Chordata/q/(\w.*)" )

	

	# -----------  SEND  -----------
	class Send_Properties(bpy.types.PropertyGroup):
		trans_method = props.EnumProperty(\
		items= u.transmition_methods() + [("none", "None", "None")] , 
		default="none")

		split_msg = props.BoolProperty( name="Split output in chunks", 
			description="""Split the retransmision on bundles containing a maximun number of messages. 
Useful when the buffer of the receiving parts overflows""",  default = False )

		chunks_n = notochord_dest_port = props.IntProperty( name="Max elements per chunk", default = 5 )

		ip_addr = props.StringProperty( default = "127.0.0.1" )
		multicast_grp_addr = props.StringProperty( default =  "239.0.0.2" )
		dest_port = props.IntProperty( default = 7000 )
		net_submask = props.StringProperty( default = "255.255.255.0" )

		relative_rotations = props.BoolProperty( name="Send rotations on the local reference frame of the bone",  default = False )


		send_root_z = props.BoolProperty( name="Send root bone height",  default = False )
		send_rot = props.BoolProperty( name="Send rotation",  default = True )
		send_pos = props.BoolProperty( name="Send position",  default = False )
		send_vel = props.BoolProperty( name="Send velocity",  default = False )
		send_dist = props.BoolProperty( name="Send Distance",  default = False )

		#Create a matrix to rotate 90deg
		_rotate_90_x = []
		for vec in Euler((pi/2, 0,0)).to_matrix().to_4x4():
			_rotate_90_x += vec.to_tuple()

		_transform_matrices = 	{"none":[1,0,0,0,	0,1,0,0, 	0,0,1,0,	0,0,0,1],
							 	"three": _rotate_90_x
							 	# "some_axis_flip": [1,0,0,0,	0,1,0,0, 	0,0,0,1,	0,0,-1,0]
							 }

		def set_transform(self, value):
			if self.which_transform == "none":
				self.b_transform_axis = False
			else:
				self.b_transform_axis = True
			self.transform_matrix = self._transform_matrices[self.which_transform]
			return None

		which_transform = props.EnumProperty(\
			items= [("none", "None", "None"), ("three", "THREE.js", "Y up and left handed")],
			default="none", update=set_transform)

		#ATTENTION!
		#this was suposed to be an arbitrary matrix, useful for rotations or axis flipping
		#BUT! at the moment is only used for rotations. See send.py:do_transform_axis() 
		transform_matrix = props.FloatVectorProperty( 
							    name="Matrix",
							    size=16,
							    subtype="MATRIX",
							    default=_transform_matrices["none"])

		b_transform_axis = props.BoolProperty( name="Should transform the axis before transmit?",  default = False )

	send = bpy.props.PointerProperty(name="Retransmit properties",
								description="Properties to be used when retransmiting OSC to another program",
								type=Send_Properties)
	# -----------  INTERNALS  -----------
	receiving = props.BoolProperty(default = False , options={'SKIP_SAVE'})
	max_ip_discover_attempts = props.IntProperty( default = 3 )
	last_check = props.IntProperty( default = 0 , options={'SKIP_SAVE'})

def target_update(self, context):
	"""TODO: this function should block from selecting a non armature typed object """
	pass


class Chord_Scene_Properties(bpy.types.PropertyGroup):

	target = bpy.props.PointerProperty(name = "Source armature object",
								description="The Armature to display the capture",
								type=bpy.types.Object)#, update=target_update

	helpers = bpy.props.PointerProperty(name = "Sensor helpers parent",
								description="An empty that have as children sensor helper objects",
								type=bpy.types.Object)#, update=target_update

	d1 = bpy.props.PointerProperty(name = "Target distance object #1",
								description="An object to reference the distance from a bone",
								type=bpy.types.Object)

	d2 = bpy.props.PointerProperty(name = "Target distance object #2",
								description="An object to reference the distance from a bone",
								type=bpy.types.Object)

	d3 = bpy.props.PointerProperty(name = "Target distance object #3",
								description="An object to reference the distance from a bone",
								type=bpy.types.Object)

# ======  End of GLOBAL PROPERTIES  =======

# =======================================
# =           BONE PROPERTIES           =
# =======================================

bpy.types.Bone.capture_bone = bpy.props.BoolProperty(name = "Capture Bone", 
	description = "Chordata will use this bone to place motion capture data", default = True)

bpy.types.PoseBone.transmit_bone = bpy.props.BoolProperty(name = "Transmit Bone", 
	description = "Output information of this bone throught OSC", default = False)

# ======  End of BONE PROPERTIES  =======


def register():
	bpy.utils.register_class(Chord_Scene_Properties)
	bpy.types.Scene.chordata = \
		bpy.props.PointerProperty(type=Chord_Scene_Properties)
	
	bpy.utils.register_module(__name__)

	if bpy.context.user_preferences.addons[__package__].preferences.debug_mode:
		faulthandler.enable()
		print("** Chordata Debug mode enable **")



def unregister():
	# bpy.utils.unregister_class(Chord_Get_Ip)
	bpy.utils.unregister_module(__name__)
	faulthandler.disable()
	# del WindowManager.chordata
	
if __name__ == "__main__":
	register()


