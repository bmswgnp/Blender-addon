# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018 Bruno Laurencich
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client.  If not, see <https://www.gnu.org/licenses/>.
#
#
# This program uses code from various sources, the default license is GNU GPLv3
# for all code, the dependencies where originally distributed as follows:
# -dnspython library: Copyright 2003-2007, 2009, 2011 Nominum, Inc. Under ISC License
# -osc4py3 library: Copyright 2013-2018 CNRS. Under CeCILL-2.1 license
# -request library: Copyright: 2017 Kenneth Reitz. Under Apache 2.0 License 
#

from sys import path
from os.path import join, abspath, dirname
path.append(join(dirname(abspath(__file__)), "dnspython"))
from dns.resolver import Resolver
from dns.exception import Timeout

#Naive mDNS resolution by "NameOfTheRose" working only on avahi running targets
#from https://stackoverflow.com/a/35853322
#(full answer containing a more complete yet NOT COMPILANT implementation at the end)  


myRes=Resolver()
myRes.nameservers=['224.0.0.251'] #mdns multicast address
myRes.port=5353 #mdns port
timeout = myRes.lifetime

def set_timeout(time = 3):
	global myRes
	global timeout
	timeout = time
	myRes.timeout = time
	myRes.lifetime = time

set_timeout()

def set_nameserver(name = '224.0.0.251', port=5353):
	global myRes
	# if name not in myRes.nameservers:
		# myRes.nameservers.append(name)

	myRes.nameservers = [name]
	myRes.port = port

def query(host = 'notochord.local', field = "A"):
	return myRes.query(host,field)

def get_ip(host = 'notochord.local'):
	try:
		return query(host)[0].to_text()
	except Timeout as e:
		print(e)
		return False
	except Exception as e:
		print(e)
		return False
	


# This code works when the target computer runs avahi, but fails when the target runs python zeroconf or the esp8266 mdns implementation. Interestingly Linux systems running avahi successfully resolve such targets (avahi apparently implementing nssswitch.conf mdns plugin and being a fuller implementation of the mdns protocol)
# In case of a naive mdns responder which, contrary to the rfc, sends its response via the mdns port, the following code (run on linux and windows and resolving linux avahi, hp printer and esp8266 targets) works for me: (and is also non-compliant as it uses the MDNS port to send the query while it is obviously NOT a full implementation)

# ```
# import socket
# import struct
# import dpkt, dpkt.dns
# UDP_IP="0.0.0.0"
# UDP_PORT=5353
# MCAST_GRP = '224.0.0.251'
# sock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
# sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# sock.bind( (UDP_IP,UDP_PORT) )
# #join the multicast group
# mreq = struct.pack("4sl", socket.inet_aton(MCAST_GRP), socket.INADDR_ANY)
# sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
# for host in ['esp01','microknoppix','pvknoppix','hprinter'][::-1]:
# #    the string in the following statement is an empty query packet
#      dns = dpkt.dns.DNS('\x00\x00\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x01')
#      dns.qd[0].name=host+'.local'
#      sock.sendto(dns.pack(),(MCAST_GRP,UDP_PORT))
# sock.settimeout(5)
# while True:
#   try:
#      m=sock.recvfrom( 1024 );#print '%r'%m[0],m[1]
#      dns = dpkt.dns.DNS(m[0])
#      if len(dns.qd)>0:print dns.__repr__(),dns.qd[0].name
#      if len(dns.an)>0 and dns.an[0].type == dpkt.dns.DNS_A:print dns.__repr__(),dns.an[0].name,socket.inet_ntoa(dns.an[0].rdata)
#   except socket.timeout:
#      break
# #DNS(qd=[Q(name='hprinter.local')]) hprinter.local
# #DNS(qd=[Q(name='pvknoppix.local')]) pvknoppix.local
# #DNS(qd=[Q(name='microknoppix.local')]) microknoppix.local
# #DNS(qd=[Q(name='esp01.local')]) esp01.local
# #DNS(an=[RR(name='esp01.local', rdata='\n\x00\x00\x04', ttl=120, cls=32769)], op=33792) esp01.local 10.0.0.4
# #DNS(an=[RR(name='PVknoppix.local', rdata='\n\x00\x00\xc2', ttl=120, cls=32769)], op=33792) PVknoppix.local 10.0.0.194
# ```

# The empty dns object was created in the above code by passing the constructor a string collected from the network using

# ```
# m0=sock.recvfrom( 1024 );print '%r'%m0[0]
# #'\xf6\xe8\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x05esp01\x05local\x00\x00\x01\x00\x01'
# ```

# This query was produced by nslookup so its id was non-zero (in this case \xf6\xe8) trying to resolve esp01.local. An dns object containing an empty query was then created by:

# ```
# dns = dpkt.dns.DNS(m0[0])
# dns.id=0
# dns.qd[0].name=''
# print '%r'%dns.pack()
# #'\x00\x00\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x01'
# ```

# The same result could also be created by:

# ```
# dns=dpkt.dns.DNS(qd=[dpkt.dns.DNS.Q(name='')])
# ```

# The dns object could also be created with non-empty query:

# ```
# dns=dpkt.dns.DNS(qd=[dpkt.dns.DNS.Q(name='esp01.local')])
# ```

# or even with multiple queries:

# ```
# dns=dpkt.dns.DNS(qd=[dpkt.dns.DNS.Q(name='esp01.local'),dpkt.dns.DNS.Q(name='esp02.local')])
# ```

# but minimal responders may fail to handle dns messages containing multiple queries


# I am also unhappy with the python zeroconf documentation. From a casual reading of the code and packet monitoring using tcpdump, it seems that (when the registration example is running) zeroconf will respond to address queries but nslookup ignores (or does not receive) the answer.
