import pytest
import mathutils
import math

@pytest.fixture
def armature_nodetree(chordata_nodetree):
	n_node = chordata_nodetree.nodes.new('NotochordNodeType')
	armature_node = chordata_nodetree.nodes.new('ArmatureNodeType')
	assert armature_node.bl_label == "Armature Node"

	socket_in = armature_node.inputs[0]
	socket_out = n_node.outputs[0]
	chordata_nodetree.links.new(socket_in, socket_out)

	return n_node, armature_node


def test_no_armature_ob(bpy_test, armature_nodetree, engine, 
						chordata_module, chordata_nodetree, copp_Q_packet):
	n_node, armature_node = armature_nodetree
	assert armature_node.name in chordata_nodetree.nodes
	assert 'Cube' in bpy_test.data.objects
	armature_node.settings.armature_ob = bpy_test.data.objects['Cube']
	
	eng_arm_node = engine.EngineNode(armature_node)

	assert eng_arm_node.armature_manager is None
	
	#When there's no armature the node should pass the packet untouched
	random_str = "8ds9dad89as8d9fdfd653"
	copp_Q_packet.random_str = random_str 
	q_packet_response = eng_arm_node(copp_Q_packet)
	assert q_packet_response.random_str == random_str

	
@pytest.fixture
def armature_node(bpy_test, armature_nodetree, engine, chordata_avatar):
	n_node, armature_node = armature_nodetree
	armature_node.settings.armature_ob = chordata_avatar

	eng_instance = engine.Engine()
	eng_instance.update_context(bpy_test.context)
	node_root = eng_instance.parse_tree(n_node)

	arm_engnode = node_root.children[0]
	return arm_engnode

def test_armature_node(armature_node, chordata_avatar):
	assert armature_node.armature_manager._object == chordata_avatar.name
	assert armature_node.armature_manager.pbones == chordata_avatar.pose.bones
	assert armature_node.armature_manager.dbones == chordata_avatar.data.bones
	assert armature_node.armature_manager.root_dbone.name == 'base' 


def test_armature_node_processing(armature_node, engine, chordata_avatar, copp_Q_packet):
	armature_node.id_settings.create_out_rot_packet = True
	q_packet_response = armature_node(copp_Q_packet)
	assert q_packet_response.target == engine.DataTarget.ROT
	# The number of messages on the ROT packet should match the 
	# amount of messages on the original packet
	elems = [0,0]
	for m in q_packet_response.get():
		elems[0] +=1

	copp_Q_packet.restore()
	for m in copp_Q_packet.get():
		elems[1] +=1	
	assert elems[0] == elems[1]

	#when not outputting the ROT packet, this node
	#should return the original packet with all the original messages 
	armature_node.id_settings.create_out_rot_packet = False
	random_str = "8ds9dad89as8d9fdfd653"
	copp_Q_packet.random_str = random_str 
	q_packet_response = armature_node(copp_Q_packet)
	assert q_packet_response.target == engine.DataTarget.Q
	assert copp_Q_packet.random_str == random_str
	elems[1] = 0
	for m in copp_Q_packet.get():
		elems[1] +=1	

	assert elems[0] == elems[1]

def test_armature_bone_groups(armature_node, engine, chordata_avatar, copp_Q_packet):
	bone_groups = armature_node.armature_manager.id_armature.pose.bone_groups
	bones = armature_node.armature_manager.id_armature.pose.bones
	for b in bones:
		if b.chordata.capture_bone:
			assert b.name in bone_groups

	armature_node(copp_Q_packet)
	armature_node.cleanup()
	assert len(bone_groups) == 0

@pytest.fixture
def copp_COV_packet(copp_packet, copp_server, engine):
    p = copp_packet()
    p.target = engine.DataTarget.EXTRA
    addrpattern = "/Chordata/extra/head/amcov"
    typetags = ",ff"
    msg = copp_server.COPP_Message(addrpattern, typetags, (0.2, 500))
    p._elements.append(msg)
    addrpattern = "/Chordata/extra/dorsal/amcov"
    msg = copp_server.COPP_Message(addrpattern, typetags, (0.2, 5000))
    p._elements.append(msg)
    return p

@pytest.fixture
def copp_EXTRA_packet(copp_packet, copp_server, engine):
    p = copp_packet()
    p.target = engine.DataTarget.EXTRA
    addrpattern = "/Chordata/extra/head/pippo"
    typetags = ",ff"
    msg = copp_server.COPP_Message(addrpattern, typetags, (0.2, 500))
    p._elements.append(msg)
    return p

def test_armature_display_colors(armature_node, chordata_defaults, chordata_avatar, 
	copp_COV_packet, copp_Q_packet, copp_EXTRA_packet, mocker):
	bone_groups = armature_node.armature_manager.id_armature.pose.bone_groups
	assert tuple(bone_groups["dorsal"].colors.normal) == pytest.approx(tuple(chordata_defaults.bone_no_info), rel=1)
	armature_node._display_colors = True
	armature_node(copp_COV_packet)
	assert tuple(bone_groups["dorsal"].colors.normal) != pytest.approx(tuple(chordata_defaults.bone_no_info), rel=1)
	
	armature_node._display_colors = True
	armature_node.id_settings.display_colors = False
	armature_node(copp_Q_packet)
	assert armature_node.armature_manager.id_armature.mode == 'OBJECT'

	armature_node._display_colors = False
	armature_node.id_settings.display_colors = True
	armature_node(copp_Q_packet)
	assert armature_node.armature_manager.id_armature.mode == 'POSE'
	
	armature_node._display_colors = False
	armature_node.armature_manager.set_color_on_bone = mocker.Mock()
	armature_node(copp_COV_packet)
	armature_node.armature_manager.set_color_on_bone.assert_not_called()

	armature_node._display_colors = True
	armature_node.id_settings.display_colors = True

	armature_node(copp_EXTRA_packet)
	armature_node.armature_manager.set_color_on_bone.assert_not_called()






@pytest.fixture
def quat_90deg_X():
	return mathutils.Quaternion(mathutils.Euler((math.pi/2, 0, 0)))

@pytest.fixture
def quat_list_avg_identity():
	from random import random, shuffle

	# some known complementary rotation pairs
	complementary_rots =  [ 
	mathutils.Quaternion(mathutils.Euler(( math.pi/2, 0, 0))),
	mathutils.Quaternion(mathutils.Euler((-math.pi/2, 0, 0))),
	mathutils.Quaternion(mathutils.Euler((0,  math.pi/2, 0))),
	mathutils.Quaternion(mathutils.Euler((0, -math.pi/2, 0)))
	]

	# add some random complementary rotation pairs
	half_pi = math.pi/2
	for i in range(100):
		vals = (random()*half_pi, random()*half_pi, random()*half_pi)
		complementary_rots.append(mathutils.Quaternion(mathutils.Euler(( vals[0], -vals[1], vals[2]))))
		complementary_rots.append(mathutils.Quaternion(mathutils.Euler(( -vals[0], vals[1], -vals[2]))))

	#return the shuffled list to test for commutativity
	shuffle(complementary_rots)
	return complementary_rots


# def test_armature_node_no_ob(armature_node):
# 	assert not armature_node.settings.armature_ob 

# ========================================
# =           ARMATURE MANAGER           =
# ========================================

def test_armature_manager_process(bpy_test, armature_node, chordata_avatar, 
										mocker, chordata_module, quat_90deg_X):
	the_bone = "dorsal"
	the_quat = quat_90deg_X
	assert chordata_avatar.pose.bones[the_bone].chordata.dirty is False 
	armature_node.armature_manager.receive_quat(the_bone, the_quat)
	assert chordata_avatar.pose.bones[the_bone].chordata.Q_temp_received == mathutils.Quaternion(the_quat)
	assert chordata_avatar.pose.bones[the_bone].chordata.dirty is True

	#It should log when wrong bone name
	original = chordata_module.utils.out.debug
	chordata_module.utils.out.debug = mocker.Mock()
	armature_node.armature_manager.receive_quat("fake", (2,3,4,5)) 
	chordata_module.utils.out.debug.assert_called_once()
	chordata_module.utils.out.debug = original

	prevs = {}
	for b in chordata_avatar.pose.bones:
		prevs[b.name] = b.matrix.copy()

	armature_node.armature_manager.process_pose( calibrating = False )

	C = bpy_test.context

	depsgraph = C.evaluated_depsgraph_get()

	object_eval = chordata_avatar.evaluated_get(depsgraph)
	childM2 = object_eval.pose.bones['l-arm'].matrix.copy()

	parent_bone = "base"
	assert object_eval.pose.bones[parent_bone].matrix == prevs[parent_bone]

	child_node = "head"
	assert object_eval.pose.bones[child_node].matrix != prevs[child_node]

# ======  End of ARMATURE MANAGER  =======

# ===================================
# =           CALIBRATION           =
# ===================================

def test_calibration_storage(armature_node, mocker, quat_90deg_X):
	the_bone = "dorsal"
	the_quat = quat_90deg_X
	arm_man = armature_node.armature_manager
	assert arm_man._calib_values == {} 
	
	stored_items = 23

	for i in range(stored_items):
		armature_node.armature_manager.receive_quat(the_bone, the_quat)
		arm_man.process_pose( calibrating = True )
	
	assert tuple(arm_man._calib_values.keys()) == (the_bone, )
	assert len(arm_man._calib_values[the_bone]) == stored_items
	assert arm_man._calib_values[the_bone].pop() == the_quat

	arm_man.process_pose( calibrating = False )
	assert arm_man._calib_values == {} 


def test_calibration_bone_states(armature_node, chordata_avatar, quat_90deg_X):
	arm_man = armature_node.armature_manager
	arm_man._calib_queue_size = 1
	bone_name = "dorsal"
	the_bone = chordata_avatar.pose.bones[ bone_name ]
	another_bone = chordata_avatar.pose.bones[ "head" ]
	the_quat = quat_90deg_X
	assert the_bone.chordata.calibration_state == "NO-CALIB"
	assert another_bone.chordata.calibration_state == "NO-CALIB"

	armature_node.armature_manager.receive_quat( bone_name , the_quat )
	arm_man.store_calibration_values()
	arm_man.calibrate()
	assert the_bone.chordata.calibration_state == "CALIBRATED"
	assert another_bone.chordata.calibration_state == "INVALID"

	#ask to calibrate again, with no calibration data
	arm_man.calibrate()
	assert the_bone.chordata.calibration_state == "OUTDATED"
	assert another_bone.chordata.calibration_state == "INVALID"


def test_calibration_result(armature_node, chordata_avatar, quat_list_avg_identity):
	arm_man = armature_node.armature_manager
	arm_man._calib_queue_size = len(quat_list_avg_identity)
	bone_name = "dorsal"
	the_bone = chordata_avatar.pose.bones[ bone_name ]
	the_bone.chordata.Q_calibration = quat_list_avg_identity[0]
	for q in quat_list_avg_identity:
		armature_node.armature_manager.receive_quat( bone_name , q )
		arm_man.store_calibration_values()

	arm_man.calibrate()
	assert the_bone.chordata.calibration_state == "CALIBRATED"
	assert the_bone.chordata.Q_avg_incoming == mathutils.Quaternion((1,0,0,0))



def test_calibration_length_control(armature_node, chordata_avatar, quat_90deg_X):
	arm_man = armature_node.armature_manager
	new_queue_size = 10
	bone_name = "dorsal"
	the_quat = quat_90deg_X

	the_bone = chordata_avatar.pose.bones[ bone_name ]
	arm_man._calib_queue_size = new_queue_size
	assert arm_man._calib_values == {}
	for i in range(new_queue_size):
		armature_node.armature_manager.receive_quat( bone_name , the_quat )
		arm_man.store_calibration_values()

	arm_man.calibrate()
	assert the_bone.chordata.calibration_state == "CALIBRATED"

	#now add less calibration values than required
	for i in range(new_queue_size - 1):
		armature_node.armature_manager.receive_quat( bone_name , the_quat )
		arm_man.store_calibration_values()

	arm_man.calibrate()
	assert the_bone.chordata.calibration_state == "OUTDATED"
	


def test_calibration_state_machine(armature_node, mocker):
	arm_man = armature_node.armature_manager
	arm_man.process_bone = mocker.Mock()
	arm_man.calibrate = mocker.Mock()
	arm_man.store_calibration_values = mocker.Mock()
	
	stored_items = 0

	arm_man.process_pose( calibrating = True )
	stored_items += 1

	arm_man.process_pose( calibrating = True )
	stored_items += 1

	arm_man.process_pose( calibrating = True )
	stored_items += 1

	arm_man.process_pose( calibrating = False )
	process_bones = 1
	arm_man.process_pose( calibrating = False )
	process_bones += 1
	arm_man.process_pose( calibrating = False )
	process_bones += 1

	assert arm_man.store_calibration_values.call_count == stored_items
	arm_man.calibrate.assert_called_once()
	assert arm_man.process_bone.call_count == process_bones


	## -- START OVER --
	arm_man.calibrate.reset_mock()
	arm_man.store_calibration_values.reset_mock()
	arm_man.process_bone.reset_mock()
	stored_items = 0
	process_bones = 0

	arm_man.process_pose( calibrating = True )
	stored_items += 1

	arm_man.process_pose( calibrating = True )
	stored_items += 1

	arm_man.process_pose( calibrating = False )
	process_bones += 1
	arm_man.process_pose( calibrating = False )
	process_bones += 1

	assert arm_man.store_calibration_values.call_count == stored_items
	arm_man.calibrate.assert_called_once()
	assert arm_man.process_bone.call_count == process_bones

	## -- START OVER --
	arm_man.calibrate.reset_mock()
	arm_man.store_calibration_values.reset_mock()
	arm_man.process_bone.reset_mock()
	stored_items = 0
	process_bones = 0

	arm_man.process_pose( calibrating = True )
	stored_items += 1

	arm_man.process_pose( calibrating = False )
	process_bones += 1
	arm_man.process_pose( calibrating = False )
	process_bones += 1

	assert arm_man.store_calibration_values.call_count == stored_items
	arm_man.calibrate.assert_called_once()
	assert arm_man.process_bone.call_count == process_bones

# ======  End of CALIBRATION  =======

