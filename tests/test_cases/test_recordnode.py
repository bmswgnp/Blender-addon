import pytest

@pytest.fixture
def recnode(chordata_nodetree, engine, chordata_avatar):
	recordnode = chordata_nodetree.nodes.new("RecordNodeType")
	recordnode.settings.target_object = chordata_avatar

	eng_instance = engine.Engine()
	eng_rec_node = eng_instance.parse_tree(recordnode)

	return eng_rec_node

def test_recnode_record(bpy_test, recnode, copp_ROT_packet, mocker):	
	recnode.id_settings.target_object.animation_data_clear()
	assert not recnode.id_settings.target_object.animation_data

	key_groups = [m.subtarget for m in copp_ROT_packet._elements]

	assert recnode.fps == bpy_test.context.scene.render.fps
	recnode.id_settings.do_record = True
	recnode.stats.ellapsed_time = mocker.Mock( return_value = 20 )
	
	#Record one keyframe
	recnode(copp_ROT_packet)
	assert recnode._recording == True
	recnode.stats.ellapsed_time.assert_called_once()

	assert recnode.id_settings.target_object.animation_data
	assert recnode.id_settings.target_object.animation_data.action

	action = recnode.id_settings.target_object.animation_data.action
	#the recorded keyframe should be set at the time the stats object returns
	assert action.fcurves[0].keyframe_points[0].co[0] == int(20 * recnode.fps)
	#the recorded keyframe should belong to a group named as the bone
	assert action.fcurves[0].group.name in key_groups


def test_recnode_preserve_last_action(bpy_test, recnode, copp_ROT_packet, mocker):	
	recnode.id_settings.do_record = True
	recnode.stats.ellapsed_time = mocker.Mock( return_value = 20 )
	
	#Record two keyframes
	recnode(copp_ROT_packet)
	copp_ROT_packet.restore()
	recnode(copp_ROT_packet)
	copp_ROT_packet.restore()

	#end recording
	recnode.id_settings.do_record = False
	recnode(copp_ROT_packet)
	copp_ROT_packet.restore()
	first_action = recnode.id_settings.target_object.animation_data.action

	#Record another keyframe
	recnode.id_settings.do_record = True
	recnode(copp_ROT_packet)
	copp_ROT_packet.restore()

	new_action = recnode.id_settings.target_object.animation_data.action
	assert new_action != first_action
	assert first_action.use_fake_user == True

def test_recnode_continue_last_action(bpy_test, recnode, copp_ROT_packet, mocker):
	recnode.id_settings.target_object.animation_data_clear()	
	recnode.id_settings.do_record = True
	recnode.id_settings.create_new_action = False
	recnode.stats.ellapsed_time = mocker.Mock( return_value = 360 )

	#Record one keyframes
	recnode(copp_ROT_packet)
	copp_ROT_packet.restore()

	#end recording
	recnode.id_settings.do_record = False
	recnode(copp_ROT_packet)
	copp_ROT_packet.restore()
	first_action = recnode.id_settings.target_object.animation_data.action
	assert first_action.frame_range[1] == int(360 * recnode.fps) + 1 

	#Record another keyframe
	recnode.stats.ellapsed_time = mocker.Mock( return_value = 183 )
	recnode.id_settings.do_record = True
	recnode(copp_ROT_packet)

	new_action = recnode.id_settings.target_object.animation_data.action
	assert new_action == first_action
	assert new_action.frame_range[1] == int( (360 + 183) * recnode.fps) + 1 
	

@pytest.fixture
def arm_and_rec_nodes(bpy_test,chordata_nodetree, engine, chordata_avatar):
	armnode = chordata_nodetree.nodes.new("ArmatureNodeType")
	recordnode = chordata_nodetree.nodes.new("RecordNodeType")
	armnode.settings.armature_ob = chordata_avatar

	socket_out = armnode.outputs['armature_out']
	socket_in = recordnode.inputs['armature_in']

	chordata_nodetree.links.new(socket_in, socket_out)

	eng_instance = engine.Engine()
	eng_instance.update_context(bpy_test.context)
	eng_arm_node = eng_instance.parse_tree(armnode)

	return eng_arm_node

def test_recnode_record_armature(bpy_test, arm_and_rec_nodes, 
									copp_Q_packet, mocker):	
	recnode = arm_and_rec_nodes.children[0]
	assert recnode.id_settings.target_object == arm_and_rec_nodes.id_settings.armature_ob 
	recnode.id_settings.target_object.animation_data_clear()
	assert not recnode.id_settings.target_object.animation_data

	#Don't record if the time from the last keyframe insertion is 
	#lower than the scene FPS
	recnode.get_frame = mocker.Mock(return_value=1)
	recnode.stats.get_checkpoint_delta = mocker.Mock(return_value=1/200) #much lower than the default scene fps
	recnode.id_settings.do_record = True
	arm_and_rec_nodes(copp_Q_packet)
	recnode.get_frame.assert_not_called()

	#Don't record if the switch is off
	recnode.stats.get_checkpoint_delta = mocker.Mock(return_value = 1)
	recnode.id_settings.do_record = False
	copp_Q_packet.restore()
	arm_and_rec_nodes(copp_Q_packet)
	recnode.get_frame.assert_not_called()

	#Record
	recnode.id_settings.do_record = True
	copp_Q_packet.restore()
	arm_and_rec_nodes(copp_Q_packet)
	recnode.get_frame.assert_called_once()

	assert recnode.id_settings.target_object.animation_data.action is not None
