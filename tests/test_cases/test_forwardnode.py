def test_init(chordata_nodetree):
    testcubenode = chordata_nodetree.nodes.new("ForwardNodeType")

    assert testcubenode.inputs[0].bl_idname == 'DataStreamSocketType'