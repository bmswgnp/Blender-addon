import pytest

def remove_all_objs(bpy):
	for ob in bpy.data.objects:
		bpy.data.objects.remove(ob)


@pytest.fixture
def vecnode(bpy_test, engine, chordata_nodetree):
	vectornode = chordata_nodetree.nodes.new("VectorNodeType")
	bpy_test.ops.object.empty_add()
	vectornode.settings.target_object = bpy_test.context.object

	vec_eng_node = engine.EngineNode(vectornode)
	yield vec_eng_node
	remove_all_objs(bpy_test)

@pytest.fixture
def copp_RAW_packet(copp_packet, copp_server, engine):
    p = copp_packet()
    p.target = engine.DataTarget.RAW
    addrpattern = "/Chordata/raw/head"
    typetags = ",iiiiiiiii"
    msg = copp_server.COPP_Message(addrpattern, typetags, (1,2,3)*3)
    p._elements.append(msg)
    addrpattern = "/Chordata/raw/dorsal"
    msg = copp_server.COPP_Message(addrpattern, typetags, (1,2,3)*3)
    p._elements.append(msg)
    addrpattern = "/Chordata/raw/base"
    msg = copp_server.COPP_Message(addrpattern, typetags, (1,2,3)*3)
    p._elements.append(msg)
    return p

def test_vector_node_no_ob(bpy_test, engine, chordata_nodetree):
	vectornode = chordata_nodetree.nodes.new("VectorNodeType")
	vec_eng_node = engine.EngineNode(vectornode)
	assert vec_eng_node.arrow is None

def test_vector_node_point_cloud(bpy_test, engine, chordata_nodetree, copp_RAW_packet):
	vectornode = chordata_nodetree.nodes.new("VectorNodeType")
	bpy_test.ops.object.empty_add()
	vectornode.settings.target_object = bpy_test.context.object
	vectornode.settings.draw_point_cloud = True
	vec_eng_node = engine.EngineNode(vectornode)
	assert vec_eng_node.point_cloud in bpy_test.data.objects

	vec_eng_node(copp_RAW_packet)
	vec_eng_node.id_settings.selected_subtarget = "head"
	copp_RAW_packet.restore()
	vec_eng_node(copp_RAW_packet)

	vec_eng_node.id_settings.selected_subtarget
	vec_eng_node.cleanup()
	assert vec_eng_node.point_cloud not in bpy_test.data.objects

	remove_all_objs(bpy_test)

def test_vector_node_cleanup(bpy_test, vecnode):
	assert vecnode.arrow in bpy_test.data.objects
	vecnode.cleanup()
	assert vecnode.arrow not in bpy_test.data.objects

def test_vector_node(bpy_test, vecnode, copp_RAW_packet):
	arrow = bpy_test.data.objects[ vecnode.arrow ]
	prev_quat = arrow.rotation_quaternion.copy() 	

	vecnode(copp_RAW_packet)
	assert "head" in vecnode.id_tree.subtargets
	assert "dorsal" in vecnode.id_tree.subtargets
	assert "base" in vecnode.id_tree.subtargets
	
	copp_RAW_packet.restore()
	vecnode.id_settings.selected_subtarget = "head"
	vecnode(copp_RAW_packet)
	assert arrow.rotation_quaternion != prev_quat

	vecnode.id_settings.vec_from_raw = "CUSTOM"
	vecnode.id_settings.custom_vec_offset = 9
	copp_RAW_packet.restore()
	vecnode(copp_RAW_packet)
	assert vecnode.id_settings.offset_in_bounds == False


