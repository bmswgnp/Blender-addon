import xml.etree.ElementTree as ET

tolerance_threshold = 85

try:
	last_coverage = ET.parse('last_coverage.xml')
	last_root = last_coverage.getroot()
	

except ET.ParseError as e:
	print("The last_coverage.xml file is malformed")
	f = open('last_coverage.xml', "r")
	print(f.read()) 
	quit(1)


last_b_rate = int( float( last_root.attrib["branch-rate"] )*100 )
last_l_rate = int( float( last_root.attrib["line-rate"] ) *100)

this_coverage = ET.parse('coverage.xml')
this_root = this_coverage.getroot()

this_b_rate = int( float( this_root.attrib["branch-rate"] )*100 )
this_l_rate = int( float(this_root.attrib["line-rate"]) *100)


pass_test = True

if last_l_rate < tolerance_threshold:
	if this_l_rate < last_l_rate:
		pass_test = False		
		print( "Line coverage decreased by {}%. (last: {}%, current: {}%)".format(
				last_l_rate - this_l_rate,
				last_l_rate, this_l_rate ))

if last_b_rate < tolerance_threshold:
	if this_b_rate < last_b_rate:
		pass_test = False		
		print( "Branch coverage decreased by {}%. (last: {}%, current: {}%)".format(
				last_b_rate - this_b_rate,
				last_b_rate, this_b_rate
				))

if not pass_test:
	print( "Coverage values below tolerance. Failing Pipeline!")
	quit(1)

print( "Line coverage change:   {:2d}%".format( this_l_rate - last_l_rate ))
print( "Branch coverage change: {:2d}%".format( this_b_rate - last_b_rate ))
