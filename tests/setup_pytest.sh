#!/bin/bash


if [ $# -lt 1 ]; then
	echo 'Usage: ./setup_pytest.sh <Blender binary path> [--with-ipython]'
	exit 1
fi

BLENDER_BIN=$1
command -v ${BLENDER_BIN}

if [ "$?" != "0" ]; then
	echo Blender binary path \[$1\] not valid
	exit 1
fi

set -eu

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
source "$SCRIPTPATH/get_blender_python_bin.sh"

BLENDPY=$(get_bundled_python_dir ${BLENDER_BIN})
BUNDLED_PY=$(get_bundled_python ${BLENDPY})

if [ "$?" != "0" ]; then
	echo $BUNDLED_PY
	exit 1
fi 

${BUNDLED_PY} -c 'print("Bundled Python interpreter working")'

${BUNDLED_PY} -m ensurepip

PIP=$(get_bundled_pip $BLENDPY)

# version 20.0 is wroken
# https://github.com/pypa/pip/issues/7217
${PIP} install --upgrade pip==20.0.1

${PIP} install numpy

${PIP} install pytest

${PIP} install pytest-mock

${PIP} install pytest-cov

if [ $# -eq 2 ] && [ "$2" == "--with-ipython" ]; then
	${PIP} install ipython
	${BUNDLED_PY} -c "import IPython; print('ipython sucesfully installed')"
fi

${BUNDLED_PY} -c "import pytest; print('pytest sucesfully installed')"
