# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from bpy.types import NodeTree, PropertyGroup
from bpy.props import CollectionProperty, StringProperty 


class ChordataTreeNode:
    @classmethod
    def poll(cls, ntree):
        return ntree.bl_idname == 'ChordataTreeType'


class Subtargets(PropertyGroup):
    name: StringProperty(name="Subtarget", default="Unknown")



class ChordataTree(NodeTree):

    '''A custom node tree type that will show up in the editor type list'''
    bl_idname = 'ChordataTreeType'

    bl_label = "Chordata Node Tree"
    # Icon identifier
    bl_icon = 'ARMATURE_DATA'

    subtargets: CollectionProperty(type=Subtargets)

    dirty = False
    engine_stop = False
    engine_running = False


to_register = ( Subtargets, ChordataTree )