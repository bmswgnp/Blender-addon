# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

bl_info = {
    "name": "Chordata motion capture client",
    "author": "Bruno Laurencich, Lorenzo Micozzi Ferri",
    "blender": (2, 80, 0),
    "version": (1, 0, 0),   #Warning addon version should be also manually 
                            #changed in ADDON_VERSION at defaults.py 
    "location": "Top Bar > Window menu > Chordata",
    "description": "A client to manage the physical motion capture performed with the Chordata system",
    "warning": "",
    "wiki_url": "http://chordata.cc",
    "tracker_url": "https://gitlab.com/chordata/Blender-addon/issues",
    "support": "COMMUNITY",
    "category": "Rigging",
}

if "bpy" in locals():
    import imp
    imp.reload(nodes)
    imp.reload(chordatatree)
    imp.reload(ops)
    imp.reload(templates)
    imp.reload(utils)
else:
    from . import nodes
    from . import chordatatree
    from . import ops
    from . import templates
    from . import utils


import bpy
from bpy.types import NodeTree, Node, NodeSocket

import nodeitems_utils


class ChordataMenu(bpy.types.Menu):
    bl_label = "Chordata"
    bl_idname = "CHORDATA_MT_menu"

    def draw(self, context):
        self.layout.operator("chordata.chordata_session_init")
        self.layout.separator(factor=1.0)
        self.layout.operator("chordata.avatar_add")
        self.layout.operator("chordata.kceptor_add")
        # self.layout.operator("chordata.fake_arm_add")
        self.layout.separator(factor=1.0)
        self.layout.operator("chordata.objects_rm")
        self.layout.separator(factor=1.0)
        self.layout.prop(bpy.context.window_manager, "chordata_debug")

def chordata_menu(self, context):
    self.layout.separator()
    self.layout.menu("CHORDATA_MT_menu", icon='ARMATURE_DATA')

to_register = chordatatree.to_register

def register():
    utils.out.create_logger()

    from bpy.utils import register_class
    for cls in to_register:
        register_class(cls)
    
    nodes.register()
    ops.register()
    templates.register()

    register_class(ChordataMenu)
    bpy.types.TOPBAR_MT_window.append(chordata_menu)

    nodeitems_utils.register_node_categories('CHORDNODES', nodes.node_categories)


def unregister():
    utils.out.clean_logger()

    nodeitems_utils.unregister_node_categories('CHORDNODES')

    from bpy.utils import unregister_class
    for cls in reversed(to_register):
        unregister_class(cls)

    nodes.unregister()
    ops.unregister()
    templates.unregister()
    
    unregister_class(ChordataMenu)
    bpy.types.TOPBAR_MT_window.remove(chordata_menu)



if __name__ == "__main__":
    register()
