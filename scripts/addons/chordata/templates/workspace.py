# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
from .. import defaults


class AddChordataWorkspace(bpy.types.Operator):
    bl_idname = "chordata.workspace_add"
    bl_label = "Add the 'Mocap' workspace"

    keep_current_workspace: bpy.props.BoolProperty(default=True)

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        active_w = context.window.workspace
        if defaults.WORKSPACE_NAME not in bpy.data.workspaces:
            bpy.ops.workspace.append_activate(idname=defaults.WORKSPACE_NAME,
                                              filepath=defaults.TEMPLATES_FILE)

        if self.keep_current_workspace:
            context.window.workspace = active_w

        return {'FINISHED'}


class RemoveChordataWorkspace(bpy.types.Operator):
    bl_idname = "chordata.workspace_rm"
    bl_label = "Remove the 'Mocap' workspace"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        self.report({'WARNING'}, """There doesn't seems to be a way to programatically remove a workspace at the moment. \
A patch on the blender source is coming soon. See console for details.""")

        print("""There doesn't seems to be a way to programatically remove a workspace at the moment.
            See: https://devtalk.blender.org/t/remove-a-workspace-without-it-being-active/5089/2 """)

        return {'CANCELLED'}

        # this is not working.
        # See: https://devtalk.blender.org/t/remove-a-workspace-without-it-being-active/5089/2
        if defaults.WORKSPACE_NAME in bpy.data.workspaces:
            chord_w = bpy.data.workspaces[defaults.WORKSPACE_NAME]
            context.window.workspace = chord_w
            context.workspace = chord_w
            bpy.ops.workspace.delete()

        return {'FINISHED'}


class ChordataSessionInit(bpy.types.Operator):
    bl_idname = "chordata.chordata_session_init"
    bl_label = "Init Chordata Mocap session"
    
    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        bpy.ops.chordata.workspace_add(
            'EXEC_DEFAULT', keep_current_workspace=False)
        bpy.ops.chordata.configuration_add('EXEC_DEFAULT')

        #force changing workspace when bpy.ops.chordata.workspace_add
        #founds it already exist
        context.window.workspace = bpy.data.workspaces[defaults.WORKSPACE_NAME]

            
        return {'FINISHED'}
