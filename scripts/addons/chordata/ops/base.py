# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
import functools
from .engine import Engine, EngineNode, ID_data_settings_getter
from ..defaults import ENGINE_REPORTS, ENGINE_CANCEL_KEYS, NODES_REDRAW_RATE
from ..utils import out 
from traceback import format_exc


def post_undoredo_reparsing(scene, operator):
    # print(operator)
    tree_exists = operator.from_tree in bpy.data.node_groups
    node_exists = operator.from_node in bpy.data.node_groups[operator.from_tree].nodes
    
    if tree_exists and node_exists:
        bpy.data.node_groups[operator.from_tree].engine_running = True
        operator.engine.parse_tree(
            bpy.data.node_groups[operator.from_tree].nodes[operator.from_node])
        
    else:
        bpy.data.node_groups[operator.from_tree].engine_running = False
        bpy.data.node_groups[operator.from_tree].engine_stop = True




class ChordataEngineManager(ID_data_settings_getter):
    
    # -----------  EXECUTE PARTIALS  -----------
    
    def check_engine_running(self):
        if self.id_tree.engine_running:
            self.report({'ERROR'}, ENGINE_REPORTS["already_running"])
            return True

        return False

    def add_undo_redo_handlers(self):
        self._post_undoredo_func = functools.partial(
            post_undoredo_reparsing, operator=self)

        # self._post_undoredo_func =   post_undoredo_reparsing  
        bpy.app.handlers.undo_post.append(self._post_undoredo_func)
        bpy.app.handlers.redo_post.append(self._post_undoredo_func)

    
    def start_timer(self, context):
        self.rate = context.scene.render.fps
        self.ticks_to_redraw =  self.rate * NODES_REDRAW_RATE
        self.tick_counter = 0 
        wm = context.window_manager
        self._timer = wm.event_timer_add(1 / self.rate, window=context.window)
        wm.modal_handler_add(self)

    def remove_timer(self, context): #pragma: no cover
        if self._timer:
            wm = context.window_manager
            wm.event_timer_remove(self._timer)



    # -----------  MODAL PARTIALS  -----------
    
    def check_and_update_tree(self):
        # Check engine stop flag, return False to stop modal if
        # the flag is set, or the node_tree is gone (deleted, or because of UNDO)
        try:
            if self.id_tree.engine_stop:
                return False
        except KeyError:
            return False

        # checks dirty flag (set when node tree is updated)
        if self.id_tree.dirty:
            self.engine.parse_tree(self.id_node)
            
        return True

    def redraw_area_on_rate(self, context): #pragma: no cover
        if self.tick_counter >= self.ticks_to_redraw:
            self.tick_counter = 0
            
            if context.area: 
                context.area.tag_redraw()

        self.tick_counter +=1

    # -----------  CLEANUP  -----------

    def cancel(self, context):
        bpy.app.handlers.undo_post.remove(self._post_undoredo_func)
        bpy.app.handlers.redo_post.remove(self._post_undoredo_func)
        
        self.remove_timer(context)

        self.engine.cleanup()
        
        try:
            # Unset running flags
            self.id_tree.engine_stop = False
            self.id_tree.engine_running = False
            
        except KeyError as e:
            self.report({'ERROR'}, ENGINE_REPORTS["node_tree_not_found"].format(self.from_tree))
            out.error(e)
            return {'CANCELLED'}


        self.report({'INFO'}, ENGINE_REPORTS["stopped"])
        return {'FINISHED'}

# ==========================================================
# =           CHORDATA NODE OPERATORS BASE CLASS           =
# ==========================================================


class BaseChordataNodeOperator(ChordataEngineManager):
    bl_options = {'REGISTER'}
    _timer = None
    from_tree: bpy.props.StringProperty()
    from_node: bpy.props.StringProperty()

    # -----------  Extensions  -----------
    
    def _execute(self, context): #pragma: no cover
        """Implement this function in derived classes to extend the execute behaviour"""
        return True

    def _modal(self, context, event): #pragma: no cover
        """Implement this function in derived classes to extend the modal behaviour"""
        return True

    def _cancel(self): #pragma: no cover
        """Implement this function in derived classes to extend the cleanup"""
        return True

    def execute(self, context):
        
        if self.check_engine_running():
            return {'CANCELLED'}

        self.add_undo_redo_handlers()

        self.engine = Engine( self.report )
        self.engine.parse_tree(self.id_node)

        if not self._execute(context):
            self.cancel(context)
            return {'CANCELLED'}
            
        self.start_timer(context)
            
        self.id_node.id_data.engine_running = True
        self.report({'INFO'}, ENGINE_REPORTS["started"])
        
        return {'RUNNING_MODAL'}


    def modal(self, context, event):
        if event.type in ENGINE_CANCEL_KEYS:
            self.cancel(context)
            return {'CANCELLED'}

        if event.type == 'TIMER':            
            if not self.check_and_update_tree():
                return self.cancel(context)

            try:
                self.engine.update_context(context)
                self._modal(context, event)
            except KeyError as e:
                self.report({'ERROR'}, ENGINE_REPORTS["key_error"].format(e))
                out.error(ENGINE_REPORTS["key_error"].format(format_exc()))
                self.cancel(context)
                return {'CANCELLED'}
            except Exception as e:
                self.report({'ERROR'}, ENGINE_REPORTS["unknown_error"].format(e, "See console for details"))
                out.error(ENGINE_REPORTS["unknown_error"].format(e, format_exc()))
                self.cancel(context)
                return {'CANCELLED'}
        
            self.redraw_area_on_rate(context)

        return {'PASS_THROUGH'}

    
    def cancel(self, context):
        super_ret = super().cancel(context)
        if not self._cancel():
         return {'CANCELLED'}

        return super_ret

# ======  End of CHORDATA NODE OPERATORS BASE CLASS  =======