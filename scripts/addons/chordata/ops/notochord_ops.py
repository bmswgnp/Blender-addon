# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
import functools
from sys import path
from .. import requests
from mathutils import Quaternion
import json
from os.path import join, abspath, dirname

from .base import BaseChordataNodeOperator
from ..copp_server import COPP_Server, COPP_Common_packet, COPP_Message, oscparse
from ..utils.networking import get_local_ip
from ..utils import out
from . import xml_generator, notochord_ops
from ..defaults import TEXTBLOCK_XML_NAME, POSE_SPACE_G_SUFX 
from .engine import Engine, EngineNode, DataTarget, ID_data_settings_getter



# def connect_request(xml, notochord_address):
#     url = 'http://' + notochord_address + '/configuration'
#     headers = {'content-type': 'xml'}
#     r = requests.get(url, headers=headers, data=xml)


class NotochordReceiveOperator(bpy.types.Operator, BaseChordataNodeOperator):
    """Parses tree and start connection with Notochord"""
    bl_idname = "chordata.notochord_receive"
    bl_label = "Notochord Receive Operator"

    def _execute(self, context):

        # config_file = self.id_settings.config_file

        # if not config_file:
        #     self.report({'ERROR'}, "Empty armature file pointer")
        #     return False

        self.copp_server = COPP_Server( port = self.id_settings.local_osc_port,
                                        logger = out.get_logger() )
        
        self.copp_server.set_base_osc_addr( self.id_settings.osc_base )
        self.copp_server.start()

        return True

    def _modal(self, context, event):
        for packet in self.copp_server.get():
            self.engine.engine_root(packet)  

    def _cancel(self):
        if hasattr(self, 'copp_server'):
            self.copp_server.close_and_wait()

class NotochordDemoOperator(bpy.types.Operator, BaseChordataNodeOperator):
    """Sends some demo data"""
    bl_idname = "chordata.notochord_demo"
    bl_label = "Notochord Demo Operator"

    data_file = join(dirname(abspath(__file__)), "Chordata_dummy_data.json")
    # base_osc_addr = "/Chordata/q"

    demo_target = DataTarget.Q

    def _execute(self, context):
        self.count = 0
        self.base_osc_addr = self.id_settings.osc_base
        COPP_Message.update_matcher(self.id_settings.osc_base)
        with open(self.data_file) as d:
            self.tosend = json.load(d)
        return True

    def create_fake_packet(self):
        packet = COPP_Common_packet()
        packet.target = self.demo_target
        for data in self.tosend[self.count % len(self.tosend)]["data"]:
            addr = "{}/{}/{}".format(self.base_osc_addr, self.demo_target.value ,data[0])
            packet._elements.append(COPP_Message(addr, ",ffff", data[1:]))
            
        return packet

    @staticmethod
    def get_quaternion_at_frame(frame, group):
        q = [0] * 4
        for fc in group.channels:
            q[fc.array_index] = fc.evaluate(frame)

        return q

    def create_action_packet(self):
        action = bpy.data.actions[self.engine.engine_root.id_settings.demo_anim]
        duration = action.frame_range[1] - action.frame_range[0]
        frame =  action.frame_range[0] + (self.count % duration)

        packet = COPP_Common_packet()
        packet.target = self.demo_target
        for target_group in action.groups:
            if not target_group.name.startswith(POSE_SPACE_G_SUFX ):
                continue
            q = self.get_quaternion_at_frame(frame, target_group)
            addr = "{}/{}/{}".format(self.base_osc_addr, self.demo_target.value, target_group.name[len(POSE_SPACE_G_SUFX ):])
            packet._elements.append(COPP_Message(addr, ",ffff", q))

        return packet

    def _modal(self, context, event):
        self.count += 1
        if self.engine.engine_root.id_settings.demo_fake:
            packet = self.create_fake_packet()
        else:
            packet = self.create_action_packet()

        self.engine.engine_root( packet )

class NotochordStopOperator(bpy.types.Operator, ID_data_settings_getter):
    """Stop engine"""
    bl_idname = "chordata.notochord_stop"
    bl_label = "Notochord Stop Operator"

    from_tree: bpy.props.StringProperty()

    def execute(self, context):
        # Stop modal only if running flag is set
        if self.id_tree.engine_running:
            self.id_tree.engine_stop = True
            return {'FINISHED'}
        else:
            self.report({'ERROR'}, "Engine not running")
            return {'CANCELLED'}


class ActionPoseRotationItem(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(name="Bone name", default="unset")
    value: bpy.props.FloatVectorProperty(name="Pose space rotation", 
        description="Rotation in pose space of the associated bone", default=(1, 0, 0, 0),
        size=4, subtype='QUATERNION')

def add_props_to_Action():
    bpy.types.Action.chordata_world_rotations = bpy.props.CollectionProperty(
        type=ActionPoseRotationItem, options={'ANIMATABLE'})

to_register = (NotochordReceiveOperator, NotochordStopOperator, NotochordDemoOperator, ActionPoseRotationItem)
