# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
import logging
from .. import defaults

def get_logger():
	return logging.getLogger( defaults.logger_name )

def _debug_global_update(self, context):
	if self.chordata_debug:
		get_logger().setLevel(logging.DEBUG)
	else:
		get_logger().setLevel(logging.INFO)

def register_debug_global(): 
	bpy.types.WindowManager.chordata_debug = bpy.props.BoolProperty( name = "Chordata debug mode",
	description = "Enables debug mode in Chordata add-on",
	update = _debug_global_update )


def create_logger():
	register_debug_global()
	logger = get_logger()
	logger.setLevel(logging.INFO)
	ch = logging.StreamHandler()
	ch.setLevel(logging.DEBUG)
	formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
	ch.setFormatter(formatter)
	logger.addHandler(ch)
	return logger


def clean_logger():
	logger = logger = get_logger()
	for han in logger.handlers:
	    logger.removeHandler(han)

def debug(*args):
	logger = logging.getLogger( defaults.logger_name )
	logger.debug(*args)

def info(*args):
	logger = logging.getLogger( defaults.logger_name )
	logger.info(*args)

def warn(*args):
	logger = logging.getLogger( defaults.logger_name )
	logger.warning(*args)

def warning(*args):
	logger = logging.getLogger( defaults.logger_name )
	logger.warning(*args)

def error(*args):
	logger = logging.getLogger( defaults.logger_name )
	logger.error(*args)


def ipython_inspect():
	import IPython
	IPython.embed()


