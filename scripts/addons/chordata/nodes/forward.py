# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy.props
from bpy.types import NodeSocket
from .basenode import ChordataBaseNode


class ForwardSettings(bpy.types.PropertyGroup):
    # === Custom Properties ========================================================
    forward_ip: bpy.props.StringProperty(
        name="Forward IP", default="127.0.0.1")
    forward_port: bpy.props.IntProperty(name="Forward Port", default=7000)
    show_advanced: bpy.props.BoolProperty(name="Show Advanced", default=False)
    send_rotation: bpy.props.BoolProperty(name="Send Rotation", default=True)
    send_position: bpy.props.BoolProperty(name="Send Position", default=False)
    send_velocity: bpy.props.BoolProperty(name="Send Velocity", default=False)
    send_acceleration: bpy.props.BoolProperty(
        name="Send Acceleration", default=False)
    transmission_mode: bpy.props.EnumProperty(
        name="Transmission mode",
        description="Network transmission mode used",
        items=[
            ("UNICAST", "Unicast", "For notochord Unicast transmission"),
            ("BROADCAST", "Broadcast", "For notochord Broadcast transmission"),
            ("MULTICAST", "Multicast", "For notochord Multicast transmission")
        ],
        default="UNICAST"
    )
    net_submask: bpy.props.StringProperty(
        name="Net Submask", default="255.255.255.0")
    multi_id_group: bpy.props.StringProperty(
        name="Multi_id Group", default="239.0.0.1")
    # ==============================================================================


class ForwardNode(ChordataBaseNode):
    '''Forwards motion capture data in the network'''
    bl_idname = 'ForwardNodeType'

    bl_label = "Forward Main Node"

    # === Property Group Pointer ===================================================
    settings: bpy.props.PointerProperty(
        type=ForwardSettings)
    # ==============================================================================


    def init(self, context):
        self.width = 350.0
        self.inputs.new('DataStreamSocketType', "data_in")

    def draw_buttons(self, context, layout):
        # Set the whole layout as inactive as lon as this node is not implemented
        layout.active = False

        if self.settings.show_advanced:
            layout.prop(self.settings,
                        "transmission_mode", expand=True)
        if self.settings.show_advanced and self.settings.transmission_mode == "BROADCAST":
            layout.prop(self.settings,
                        "net_submask", text="Net Submask")
        elif self.settings.show_advanced and self.settings.transmission_mode == "MULTICAST":
            layout.prop(self.settings,
                        "multi_id_group", text="Multi_id Group")
        if self.settings.transmission_mode == "UNICAST":
            layout.prop(self.settings, "forward_ip", text="Forward IP")
        layout.prop(self.settings, "forward_port", text="Forward Port")
        layout.prop(self.settings, "show_advanced")
        if self.settings.show_advanced:
            layout.prop(self.settings, "send_rotation")
            layout.prop(self.settings, "send_position")
            layout.prop(self.settings, "send_velocity")
            layout.prop(self.settings, "send_acceleration")


    def draw_label(self):
        return "Forward Node"


to_register = (ForwardSettings, ForwardNode,)
