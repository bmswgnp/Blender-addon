# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy.props
from bpy.types import NodeSocket
from .basenode import ChordataBaseNode, set_dirty_flag
from ..copp_server import COPP_Server, COPP_Common_packet, COPP_Message, oscparse
from .. import defaults
from ..ops import engine, armature_manager


def check_arm_ob(self, context):
    if self.armature_ob and self.armature_ob.type == 'ARMATURE':
        self.valid_arm_ob = True
    else:
        self.valid_arm_ob = False

    set_dirty_flag(self, context)



class ArmatureSettings(bpy.types.PropertyGroup):
    # === Custom Properties ========================================================
    valid_arm_ob: bpy.props.BoolProperty(name="Valid armature object", default=False)
    
    armature_ob: bpy.props.PointerProperty(
        type=bpy.types.Object, name="Armature", update=check_arm_ob)
    
    show_advanced: bpy.props.BoolProperty(name="Show Advanced", default=False)

    calc_root_z: bpy.props.BoolProperty(name="Root height", default=True)

    create_out_rot_packet: bpy.props.BoolProperty(default=False)

    # -- Mag dovariance colors --

    display_colors: bpy.props.BoolProperty(name="Display colors (enter pose-mode)", 
        description = "Enter pose mode to see colored bones acording to the magnetic distortion", default=True)
    
    mag_cov_range: bpy.props.IntVectorProperty(name="Magnetic covariance display range", 
        description = "Range to display as magnetic covariance error", size=2, default=(20,1000))

    # -- Calibration props --

    doing_calibration: bpy.props.BoolProperty(name="Calibrate", 
        description = "Click to calibrate your skeleton", default=False)

    calib_queue_size: bpy.props.IntProperty(name="Calibration samples", 
        description = "Number of samples to collect for calibration", 
        default=defaults.CALIB_QUEUE_SIZE )

    timed_calib_delay: bpy.props.IntProperty(name="Timed calibration delay", 
        description = "Seconds to wait before calibrating", 
        default=3 )

    timed_calib_time: bpy.props.IntProperty(name="Timed calibration seconds", 
        description = "Timed calibration duration", 
        default=3 )

    doing_timed_calibration: bpy.props.BoolProperty(default=False)
    timed_calibration_secs: bpy.props.FloatProperty()


    # -- Filters --
    smooth_factor: bpy.props.FloatProperty(name="Smooth factor", 
        description = "the smoothin factor applied to incoming rotations", 
        default=0.5, min=0, max=0.999 )
    
    canonicize_quats: bpy.props.BoolProperty(name="Canonicize quaternions", 
        description = "Keep the a 'canonized' form of quaternion to avoid interpolation issues.", 
        default=True)
    
    # ==============================================================================


class ArmatureNode(ChordataBaseNode):

    '''Chordata Armature Node'''
    bl_idname = 'ArmatureNodeType'

    bl_label = "Armature Node"

    # === Property Group Pointer ===================================================
    settings: bpy.props.PointerProperty(
        type=ArmatureSettings)
    # ==============================================================================

    def init(self, context):
        self.width = 350.0
        self.inputs.new('DataStreamSocketType', "capture_in")
        self.outputs.new('DataStreamSocketType', "data_out")
        self.outputs.new('ArmatureObjectSocketType', "armature_out")

    def update(self):
        super().update()
        self.settings.create_out_rot_packet = True
        for inp in self.outputs:
            for link in inp.links:
                # if link.to_node.bl_idname == 'RecordNodeType'
                if link.from_socket.bl_idname == 'ArmatureObjectSocketType':
                    self.settings.create_out_rot_packet = False

        
    def draw_buttons(self, context, layout):
        first_row = layout.column()
        first_row.alert = not self.settings.valid_arm_ob
        first_row.prop(self.settings, "armature_ob", text="Armature")
        if not self.settings.valid_arm_ob and self.settings.armature_ob:
            first_row.label(text="The selected object is not of Armature type")
        
        layout_area = layout.column()
        layout_area.active = self.settings.valid_arm_ob
        row = layout_area.row()
        row.prop(self.settings, "doing_calibration", text="Calibrate", expand=True, icon='POSE_HLT')
        row.scale_y = 1.5
        row.active = True
        if self.settings.doing_timed_calibration:
            row.active = False

        if self.settings.valid_arm_ob:
            layout_area.label(text="Last calibration: {}"\
                .format(self.settings.armature_ob.chordata.last_calibration_date))
        

        layout_area.separator()
        layout_area.prop(self.settings, "show_advanced",text="Advanced controls")
        if self.settings.show_advanced:
            layout_area.label(text="Advanced Calibration")
            delete_calib_op = layout_area.operator("chordata.armature_clear_calib",
                                      text="Reset calibration")
            delete_calib_op.armature = self.settings.armature_ob.name
            op_row = layout_area.row()
            timed_calib_op = op_row.operator("chordata.armature_timed_calib",
                                      text="Timed calibration", icon='MOD_TIME')
            op_row.scale_y = 1.5
            timed_calib_op.delay = self.settings.timed_calib_delay
            timed_calib_op.calib_time = self.settings.timed_calib_time
            timed_calib_op.from_node = self.name
            timed_calib_op.from_tree = self.id_data.name
            if self.settings.doing_timed_calibration:
                op_row.alert = True
                row = layout_area.row()
                row.alert = True
                row.scale_y = 1.5
                if self.settings.doing_calibration:
                    s = "==> Stand still for: {:.0f} <=="
                else:
                    s = "Waiting for calibration: {:.0f}"
                row.label(text=s.format(self.settings.timed_calibration_secs))                

            row = layout_area.row()
            row.prop(self.settings, "timed_calib_delay")
            row.prop(self.settings, "timed_calib_time")


    def draw_buttons_ext(self, context, layout):
        layout.label(text="Armature display")
        if self.settings.valid_arm_ob:
            layout.prop(self.settings.armature_ob.data, "display_type")
        layout.prop(self.settings, "display_colors")
        layout.prop(self.settings, "mag_cov_range")

        layout.separator()
        layout.label(text="Capture parameters")
        layout.prop(self.settings, "smooth_factor")
        layout.prop(self.settings, "calc_root_z")
        layout.prop(self.settings, "canonicize_quats")
        layout.prop(self.settings, "create_out_rot_packet")


    def draw_label(self):
        if self.settings.show_advanced:
            return "Armature Node (Advanced)"
        else:
            return "Armature Node"


    # ======================================
    # =           ARMATURE LOGIC           =
    # ======================================
    
    def on_engine_node_init(self):
        self.output_rot = False
        self._display_colors = False
        try:
            self.armature_manager = armature_manager.Armature_Manager( \
                        self.id_settings.armature_ob, \
                        self.id_settings.calib_queue_size )

            self.output_rot = True
            self.rot_target = engine.DataTarget.ROT
            self.base_osc_addr = '/Chordata'

        except armature_manager.Armature_error as e:
            self.armature_manager = None
            self.report({'WARNING'}, "Armature node could not run, reason: {}".format(e))

    def on_engine_node_stop(self):
        if self.armature_manager: #pragma: no branch
            self.armature_manager.remove_bone_groups()
            self.armature_manager.set_mode(False)

    @engine.datatarget_handler(engine.DataTarget.Q)
    def process_quats(self, packet):
        if not self.armature_manager:
            return packet

        self.armature_manager.update_ID_data_refs()
        self.armature_manager.set_context(self.engine.context)

        if self.id_settings.display_colors != self._display_colors:
            self._display_colors = self.id_settings.display_colors
            if self._display_colors:
                self.armature_manager.set_mode('POSE')
            else:
                self.armature_manager.set_mode('OBJECT')
                

        incoming_keys = []
        for msg in packet.get():
            self.armature_manager.receive_quat(msg.subtarget, msg.payload)
            incoming_keys.append(msg.subtarget)


        self.armature_manager.process_pose( self.id_settings.smooth_factor, 
                                            self.id_settings.doing_calibration,
                                            self.id_settings.calc_root_z,
                                            self.id_settings.canonicize_quats )

        if not self.id_settings.create_out_rot_packet:
            return packet.restore()
        else:
            packet = COPP_Common_packet()
            packet.target = engine.DataTarget.ROT
            
            for subtarget in incoming_keys:
                if subtarget in self.armature_manager.pbones: #pragma: no branch
                    addr = "{}/rot/{}".format(self.base_osc_addr, subtarget)
                    bone = self.armature_manager.pbones[subtarget]
                    packet._elements.append(COPP_Message(addr, ",ffff", tuple(bone.rotation_quaternion)))

            return packet


    @engine.datatarget_handler(engine.DataTarget.EXTRA)
    def process_extra(self, packet):
        if self._display_colors:
            arm = self.id_settings.armature_ob
            for msg in packet.get():
                if msg.addrtail == "amcov":
                    self.armature_manager.set_color_on_bone(msg.subtarget, 
                                                            msg.payload[1], 
                                                            self.id_settings.mag_cov_range)

    # ======  End of ARMATURE LOGIC  =======

to_register = (ArmatureSettings, ArmatureNode,)
