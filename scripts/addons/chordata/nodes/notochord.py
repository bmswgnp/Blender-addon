# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
import bpy.props
from bpy.types import NodeSocket
from .basenode import ChordataBaseNode, set_dirty_flag
from ..ops import engine
from .. import defaults

def create_anims_enum(self, context):
    items = []

    sufx_len = len(defaults.DEMO_ANIM_SUFX)
    for anim in bpy.data.actions:
        if anim.is_chordata_item:
            items.append((anim.name, anim.name[sufx_len:], "Full body mocap animation"))
    
    items.append(("FAKE-ARM", "Fake arm", "A perfecly looping, synthetic arm animation"))
    
    return items

def check_fake_action(self, context):
    if self.demo_anim == "FAKE-ARM":
        self.demo_fake = True
    else:
        self.demo_fake = False

    set_dirty_flag(self, context)


class NotochordSettings(bpy.types.PropertyGroup):
    # === Custom Properties ========================================================
    local_address: bpy.props.StringProperty(
        name="Local IP address", default="127.0.0.1"
    )
    local_osc_port: bpy.props.IntProperty(
        name="Local OSC Port", default=6565)
    config_file: bpy.props.PointerProperty(
        name="Armature Config", type=bpy.types.Text)
    notochord_address: bpy.props.StringProperty(
        name="Notochord Address", default="127.0.0.1")
    show_advanced_network: bpy.props.BoolProperty(
        name="Show Advanced Network", default=False)
    show_advanced: bpy.props.BoolProperty(
        name="Show Advanced", default=False)

    # advanced network
    transmission_mode: bpy.props.EnumProperty(
        name="Transmission mode",
        description="Network transmission mode used",
        items=[
            ("UNICAST", "Unicast", "For notochord Unicast transmission"),
            ("BROADCAST", "Broadcast", "For notochord Broadcast transmission"),
            ("MULTICAST", "Multicast", "For notochord Multicast transmission")
        ],
    )
    notochord_port: bpy.props.IntProperty(name="Notochord Port", default=80)
    net_submask: bpy.props.StringProperty(
        name="Net Submask", default="255.255.255.0")
    multi_id_group: bpy.props.StringProperty(
        name="Multi_id Group", default="239.0.0.1")

    # advanced
    kc_revision: bpy.props.StringProperty(name="KC_revision", default="2")
    log: bpy.props.StringProperty(name="Log", default="stdout,file")
    transmit: bpy.props.StringProperty(name="Transmit", default="osc")
    send_rate: bpy.props.IntProperty(
        name="Send_rate", default=50, min=1, max=100)
    verbosity: bpy.props.IntProperty(name="Verbosity", default=0, min=0, max=2)
    adapter: bpy.props.StringProperty(name="Adapter", default="/dev/i2c-1")
    osc_base: bpy.props.StringProperty(name="Osc_base", default="/Chordata")

    manual_override: bpy.props.BoolProperty(
        name="Manual Notochord Override", default=False)

    # Demo mode
    demo_mode: bpy.props.BoolProperty(
        name="Demo Mode", default=True, update=set_dirty_flag)
    
    demo_anim: bpy.props.EnumProperty(
            name="Demo action",
            description="Action to output in Demo mode",
            # update=set_dirty_flag,
            items=create_anims_enum,
            update=check_fake_action
    )

    demo_fake: bpy.props.BoolProperty(default=True)
     

    # ==============================================================================


class NotochordNode(ChordataBaseNode):

    '''Chordata Notochord Node'''
    bl_idname = 'NotochordNodeType'

    bl_label = "Notochord Node"

    # === Property Group Pointer ===================================================
    settings: bpy.props.PointerProperty(
        type=NotochordSettings)
    # ==============================================================================

    def init(self, context):
        self.width = 550.0
        self.outputs.new('DataStreamSocketType', "capture_out")
        self.init_stats()

        chordata_demo_action = False
        for anim in bpy.data.actions:
            if anim.is_chordata_item:
                chordata_demo_action = anim.name
                break

        if not chordata_demo_action:
            bpy.ops.chordata.actions_add('EXEC_DEFAULT')
            for anim in bpy.data.actions:
                if anim.is_chordata_item:
                    chordata_demo_action = anim.name
                    break

        self.settings.demo_anim = chordata_demo_action
        self.settings.demo_fake = not bool(chordata_demo_action)

    # Free function to clean up on removal.
    def free(self):
        node_tree = self.id_data
        if node_tree.engine_running:
            bpy.ops.chordata.notochord_stop(from_tree=node_tree.name)

    def draw_buttons(self, context, layout):
        row = layout.row(align=True)
        row.scale_y = 1.5
        col_receive = row.column()
        col_stop = row.column()
        layout.separator()
        if self.id_data.engine_running:
            col_receive.active = False
            col_stop.active = True
        else:
            col_receive.active = True
            col_stop.active = False

        if not (self.settings.manual_override or self.settings.demo_mode):

            receive_op = col_receive.operator("chordata.notochord_receive",
                                      text="Connect")
            receive_op.from_tree = self.id_data.name
            receive_op.from_node = self.name

            stop_op = col_stop.operator(
                "chordata.notochord_stop", text="Disconnect")
            stop_op.from_tree = self.id_data.name

            if self.settings.show_advanced_network:
                layout.prop(
                    self.settings, "transmission_mode", expand=True)
            if self.settings.show_advanced_network and self.settings.transmission_mode == "UNICAST":
                layout.prop(self.settings,
                            "local_address", text="Local Ip Address")
            elif self.settings.show_advanced_network and self.settings.transmission_mode == "BROADCAST":
                layout.prop(self.settings, "net_submask",
                            text="Net Submask")
            elif self.settings.show_advanced_network and self.settings.transmission_mode == "MULTICAST":
                layout.prop(self.settings, "multi_id_group",
                            text="Multi_id Group")
            layout.prop(self.settings, "local_osc_port",
                        text="Local OSC receiving Port")
            layout.separator()
            layout.prop(self.settings, "config_file",
                        text="Armature Config")
            layout.prop(self.settings, "notochord_address",
                        text="Notochord Address")
            if self.settings.show_advanced_network:
                layout.prop(self.settings, "notochord_port",
                            text="Notochord Port")
            
            layout.prop(self.settings, "show_advanced_network")
            layout.prop(self.settings, "show_advanced")
            if self.settings.show_advanced:
                layout.label(
                    text="CHANGE THESE ONLY IF YOU KNOW WHAT YOU'RE DOING!")
                layout.label(
                    text="Notochord currently in Manual Override Mode")
                layout.prop(self.settings, "adapter")
                layout.prop(self.settings, "kc_revision")
                layout.prop(self.settings, "log")
                layout.prop(self.settings, "transmit")
                layout.prop(self.settings, "send_rate")
                layout.prop(self.settings, "verbosity")
                layout.prop(self.settings, "osc_base")
                layout.prop(self.settings, "manual_override")
                
        elif self.settings.manual_override:
            layout.prop(self.settings, "local_osc_port",
                        text="Local OSC receiving Port")
            layout.prop(self.settings,
                        "manual_override")
        elif self.settings.demo_mode:
            layout.label(text="Demo mode is activaded")
            layout.label(
                text="In this mode the notochord node will output a pre-recorded sample capture")
            receive_op = col_receive.operator("chordata.notochord_demo",
                                      text="Run Demo")
            layout.prop(self.settings, "demo_anim")

            receive_op.from_tree = self.id_data.name
            receive_op.from_node = self.name

            stop_op = col_stop.operator(
                "chordata.notochord_stop", text="Disconnect")
            stop_op.from_tree = self.id_data.name

        layout.prop(self.settings, "demo_mode",
                            expand=True, icon='CON_FOLLOWPATH')

    def draw_label(self):
        if self.settings.manual_override:
            return "Notochord Node (Manual Override ON!)"
        elif self.settings.show_advanced:
            return "Notochord Node (Advanced)"
        else:
            return "Notochord Node"

    @engine.datatarget_handler(engine.DataTarget.Q)
    def q_handler(self, packet):
        return packet

    
to_register = (NotochordSettings, NotochordNode)
