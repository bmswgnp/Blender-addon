# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from bpy.types import NodeSocket, PropertyGroup
from bpy.props import PointerProperty, BoolProperty, FloatProperty, IntProperty
from ... import defaults

class ChordataNodeBaseProps(PropertyGroup):
    track_stats: BoolProperty(
        name="Track stats", default=True)

    packet_rate: FloatProperty(
        name="Packet rate", default=-1)

    packets_n: IntProperty(
        name="Packets received", default=0)

    msgs_n: IntProperty(
        name="Messages received", default=0)


class StatsSocket(NodeSocket):
    '''Socket outputting stats about about the node'''
    bl_idname = 'StatsSocketType'
    bl_label = "Stats Node Socket"

    config: PointerProperty(
        type=ChordataNodeBaseProps)

    def draw(self, context, layout, node, text):
        layout.label(text="Stats")

    def draw_color(self, context, node):
        return defaults.color_vio 

to_register = (ChordataNodeBaseProps, StatsSocket)
