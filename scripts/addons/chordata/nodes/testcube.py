# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy.props
from bpy.types import NodeSocket
from .basenode import ChordataBaseNode, set_dirty_flag
from .. import defaults
from ..utils import out
from ..ops import engine
import mathutils


def target_object_changed(self, context):
    if self.target_object != None:
        self.target_object.rotation_mode = 'QUATERNION'
        self.target_object_name = self.target_object.name 
    set_dirty_flag(self, context)


def create_subtargets_enum(self, context):
    items = [("no-target",) * 3]

    for subtarget in self.id_data.subtargets:
        items.append((subtarget.name, subtarget.name, subtarget.name))
    return items


class TestCubeSettings(bpy.types.PropertyGroup):
    # === Custom Properties ========================================================
    target_object: bpy.props.PointerProperty(
        type=bpy.types.Object, name="Target Object", update=target_object_changed)

    target_object_name : bpy.props.StringProperty()
        
    selected_subtarget: bpy.props.EnumProperty(options={'SKIP_SAVE'},
                                          items=create_subtargets_enum, name="Subtarget", default=None, update=set_dirty_flag)

    # ==============================================================================


class TestCubeNode(ChordataBaseNode):
    '''Chordata Armature Node'''
    bl_idname = 'TestCubeNodeType'

    bl_label = "Test Cube Node"

    # === Property Group Pointer ===================================================
    settings: bpy.props.PointerProperty(
        type=TestCubeSettings)
    # ==============================================================================

    def init(self, context):
        self.width = 400.0
        self.inputs.new('DataStreamSocketType', "capture_in")
        self.outputs.new('DataStreamSocketType', "data_out")

    def draw_buttons(self, context, layout):
        layout.prop(self.settings,
                    "target_object", text="Object")
        layout.prop(self.settings,
                    "selected_subtarget", text="Sub-target")


    def draw_label(self):
        if self.settings.target_object:
            return "Test Cube [{} -> {}]".format(self.settings.selected_subtarget, self.settings.target_object.name)

        return "Test Cube"


    @engine.helper_method
    def add_subtarget(self, subt):
        if subt in self.id_tree.subtargets:
            return

        added = self.id_tree.subtargets.add()
        added.name = subt


    @engine.datatarget_handler(engine.DataTarget.ROT)
    @engine.datatarget_handler(engine.DataTarget.Q)
    def rotate_object(self, packet):
        ob = None
        if self.id_settings.target_object:
            ob = bpy.data.objects[self.id_settings.target_object_name]

        for msg in packet.get():
            self.add_subtarget(msg.subtarget)

            if ob and self.id_settings.selected_subtarget == msg.subtarget:
                ob.rotation_quaternion = msg.payload
        
        return packet.restore()

to_register = (TestCubeSettings, TestCubeNode,)
