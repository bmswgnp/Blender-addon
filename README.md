# Chordata client [Blender Addon]
_Motion capture manager for the Chordata Open Source motion capture system_

This software allows you to receive, record, and retransmit physical motion capture data coming from a [Chordata open source motion capture system](http://chordata.cc)

[![pipeline status](https://gitlab.com/chordata/Blender-addon/badges/v1.0.0/pipeline.svg)](https://gitlab.com/chordata/Blender-addon/pipelines) [![coverage report](https://gitlab.com/chordata/Blender-addon/badges/v1.0.0/coverage.svg)](https://gitlab.com/chordata/Blender-addon/-/jobs/artifacts/v1.0.0/file/tests/htmlcov/index.html?job=coverage)

## New version

![new addon nodes](https://chordata-files.s3.eu-west-1.amazonaws.com/2019-12-03/1575388071-967903-chordatablendercustomnodesmockup100.png)

This is the branch for the upcoming `v1.0.0` version. It will feature a completely re-designed user interface, based on a node system, among many other changes.

Follow [this thread on our forum to know more](https://forum.chordata.cc/d/62-new-blender-2-8-chordata-node-system-addon)


### Cloning from git
If you want to clone this repository don't forget to pull the content from the dependencies included as git submodules

```bash
git submodule init
git submodule update --recursive --remote
```

### LICENSE

This program is free software and is distributed under the GNU General Public License, version 3.
You are free to run, study, share and modify this software.
Derivative work can only be distributed under the same license terms, and replicating this program copyright notice.

More info about the terms of the license [here](https://www.gnu.org/licenses/gpl-faq.en.html). 